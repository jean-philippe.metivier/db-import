<?php

namespace UnicaenDbImport\Domain;

use Doctrine\DBAL\Connection;

interface DestinationInterface
{
    /**
     * Retourne le petit nom de cette destination.
     *
     * @return string
     */
    public function getName();

    /**
     * Retourne le nom de la table de destination.
     *
     * @return string
     */
    public function getTable();

    /**
     * Retourne l'instance de la connexion Doctrine vers cette destination.
     *
     * @return Connection
     */
    public function getConnection();

    /**
     * Retourne le nom de la colonne commune existant obligatoirement dans la table/vue source
     * ET la table destination.
     *
     * Dans les tables source et destination, cette colonne est celle d'un identifiant unique permettant
     * de rapprocher les enregistrements source et destination.
     *
     * @return string Exemple: 'code'
     */
    public function getSourceCodeColumn();

    /**
     * Retourne les noms des colonnes de la table destination concernées par l'import.
     *
     * Ne doit pas inclure la colonne commune retournée par {@link getSourceCodeColumn}.
     *
     * @return array Exemple: ['libelle', 'debut_validite', 'fin_validite']
     */
    public function getColumns();

    /**
     * Retourne pour chaque colonne concernée par l'import l'éventuel format "sprintf" à utiliser pour transformer
     * sa valeur en chaîne de caractères.
     *
     * @return array Exemple: ['debut_validite' => "TO_CHAR(%s,'YYYY-MM-DD')", 'fin_validite' => "TO_CHAR(%s,'YYYY-MM-DD')"]
     */
    public function getColumnsToChar();
}