<?php

namespace UnicaenDbImport\CodeGenerator\Common;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use UnicaenApp\Exception\RuntimeException;
use UnicaenApp\Util;
use UnicaenDbImport\CodeGenerator\Common\Helper\FunctionCallingHelper;
use UnicaenDbImport\CodeGenerator\Common\Helper\FunctionCreationHelper;
use UnicaenDbImport\CodeGenerator\Common\Helper\TableValidationHelper;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\SourceInterface;

/**
 * Classe mère des générateurs de code SQL, quelque soit la plateforme de base de données.
 *
 * @author Unicaen
 */
abstract class AbstractCodeGenerator implements CodeGeneratorInterface
{
    /**
     * @var AbstractPlatform
     */
    protected $platform;

    /**
     * @var TableValidationHelper
     */
    protected $tableValidationHelper;

    /**
     * @var FunctionCreationHelper
     */
    protected $functionCreationHelper;

    /**
     * @var FunctionCallingHelper
     */
    protected $functionCallingHelper;

    /**
     * AbstractCodeGenerator constructor.
     *
     * @param TableValidationHelper  $tableValidationHelper
     * @param FunctionCreationHelper $functionCreationHelper
     * @param FunctionCallingHelper  $functionCallingHelper
     */
    public function __construct(TableValidationHelper $tableValidationHelper,
                                FunctionCreationHelper $functionCreationHelper,
                                FunctionCallingHelper $functionCallingHelper)
    {
        $this->tableValidationHelper = $tableValidationHelper;
        $this->functionCreationHelper = $functionCreationHelper;
        $this->functionCallingHelper = $functionCallingHelper;
    }

    /**
     * @param string $tableName
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForTableExistenceCheck($tableName)
    {
        return $this->tableValidationHelper->generateSQLForTableExistenceCheck($tableName);
    }

    /**
     * @param array $result
     * @return bool
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertTableExistenceCheckResultToBoolean(array $result)
    {
        return $this->tableValidationHelper->convertTableExistenceCheckResultToBoolean($result);
    }

    /**
     * @param string $tableName
     * @param array  $columns
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForDataColumnsValidation($tableName, array $columns)
    {
        return $this->tableValidationHelper->generateSQLForColumnsValidation($tableName, $columns);
    }

    /**
     * @param string $tableName
     * @param array  $result
     * @param array  $expectedColumns
     * @return null|RuntimeException
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertDataColumnsValidationBadResultToException($tableName, array $result, array $expectedColumns)
    {
        return $this->tableValidationHelper->convertColumnsValidationResultToException($tableName, $result, $expectedColumns);
    }

    /**
     * @param string $tableName
     * @return string
     * @codeCoverageIgnore Car simple délégation
     */
    public function generateSQLForHistoColumnsValidation($tableName)
    {
        return $this->tableValidationHelper->generateSQLForHistoColumnsValidation($tableName);
    }

    /**
     * @param string $tableName
     * @param array $result
     * @return null|RuntimeException
     * @codeCoverageIgnore Car simple délégation
     */
    public function convertHistoColumnsValidationBadResultToException($tableName, array $result)
    {
        return $this->tableValidationHelper->convertHistoColumnsValidationBadResultToException($tableName, $result);
    }

    /**
     * @param SourceInterface $source
     * @return string
     */
    public function generateSQLForSelectFromSource(SourceInterface $source)
    {
        if ($source->getSelect()) {
            return $source->getSelect();
        }

        $sourceTable = $source->getTable();
        $commaSeparatedColumnNames = implode(', ', array_merge([$source->getSourceCodeColumn()], $source->getColumns()));

        $sqlTemplate = <<<'EOT'
select {commaSeparatedColumnNames} from {sourceTable};
EOT;
        $sql = Util::tokenReplacedString($sqlTemplate, compact('sourceTable', 'commaSeparatedColumnNames'));

        return $sql;
    }

    /**
     * @param SourceInterface      $source
     * @param DestinationInterface $destination
     * @param string               $intermediateTable
     * @return string
     */
    public function generateSQLForIntermediateTableCreation(SourceInterface $source, DestinationInterface $destination, $intermediateTable)
    {
        $destinationTable = $destination->getTable();
        $sourceCodeColumn = $source->getSourceCodeColumn();
        $columns = $source->getColumns();

        $commaSeparatedColumnNames = implode(', ', array_merge([$sourceCodeColumn], $columns));

        $sqlTemplate = <<<'EOT'
create table {intermediateTable} as select {commaSeparatedColumnNames} from {destinationTable};
delete from {intermediateTable};
EOT;
        $sql = Util::tokenReplacedString($sqlTemplate, compact('intermediateTable', 'destinationTable', 'commaSeparatedColumnNames'));

        return $sql;
    }

    public function generateSQLForInsertIntoIntermmediateTable($intermediateTable, array $columnsValues)
    {
        $platform = $this->platform;

        $commaSeparatedColumnNames = implode(', ', array_keys($columnsValues));
        $commaSeparatedColumnValues = implode(', ', array_map(function($value) use ($platform) {
            if ($value === null) {
                return 'NULL';
            }
            return $platform->quoteStringLiteral($value);
        }, $columnsValues));

        $sqlTemplate = <<<'EOT'
insert into {intermediateTable} ({commaSeparatedColumnNames}) values ({commaSeparatedColumnValues});
EOT;
        $sql = Util::tokenReplacedString($sqlTemplate, compact('intermediateTable', 'commaSeparatedColumnNames', 'commaSeparatedColumnValues'));

        return $sql;
    }

    /**
     * @return string
     */
    abstract public function generateSQLForImportRegTableCreation();

    /**
     * @param string $tableName
     * @return string
     */
    abstract public function generateSQLForIntermmediateTableDrop($tableName);

    /**
     * @param \UnicaenDbImport\Domain\SourceInterface $source
     * @param DestinationInterface                    $destination
     * @return string
     */
    abstract public function generateImportMetaRequestFunctionCreationSQL(SourceInterface $source,
                                                                          DestinationInterface $destination);

    /**
     * @param SourceInterface      $source
     * @param DestinationInterface $destination
     * @param string               $intermediateTable
     * @param string               $importHash
     * @return string
     */
    abstract public function generateSourceAndDestinationDiffSelectSQL(SourceInterface $source,
                                                                       DestinationInterface $destination,
                                                                       $intermediateTable = null,
                                                                       $importHash = null);
    /**
     * @return string
     * @codeCoverageIgnore
     */
    public function generateSQLForFetchingImportRequestsToExecute()
    {
        $sql = '';
        $sql .= 'SELECT operation, table_name, field_name, from_value, to_value, sql, created_on, executed_on ';
        $sql .= 'FROM import_reg ';
        $sql .= 'WHERE executed_on IS NULL ';
        $sql .= 'ORDER BY operation, table_name, field_name ;';

        return $sql;
    }
}