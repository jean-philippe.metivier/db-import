<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use UnicaenDbImport\CodeGenerator\Common\AbstractCodeGenerator;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\FunctionCallingHelper;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\FunctionCreationHelper;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\TableValidationHelper;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\SourceInterface;

/**
 * Version PostgreSQL.
 *
 * @author Unicaen
 * @todo N'utiliser que des helpers de génération de code car quel est la différence entre CodeGenerator et un helper ?
 */
class CodeGenerator extends AbstractCodeGenerator
{
    /**
     * @var PostgreSqlPlatform
     */
    protected $platform;

    /**
     * CodeGenerator constructor.
     *
     * @param TableValidationHelper  $tableValidationHelper
     * @param FunctionCreationHelper $functionCreationHelper
     * @param FunctionCallingHelper  $functionCallingHelper
     */
    public function __construct(TableValidationHelper $tableValidationHelper,
                                FunctionCreationHelper $functionCreationHelper,
                                FunctionCallingHelper $functionCallingHelper)
    {
        parent::__construct($tableValidationHelper, $functionCreationHelper, $functionCallingHelper);

        $this->platform = new PostgreSqlPlatform();
    }

    /**
     * @param \UnicaenDbImport\Domain\SourceInterface $source
     * @param DestinationInterface                    $destination
     * @return string
     */
    public function generateImportMetaRequestFunctionCreationSQL(SourceInterface $source,
                                                                 DestinationInterface $destination)
    {
        $destinationTable = $destination->getTable();
        $sourceCodeColumn = $source->getSourceCodeColumn();
        $columns = $source->getColumns();

        $sql = $this->functionCreationHelper->generateSQL($destinationTable, $sourceCodeColumn, $columns);

        return $sql;
    }

    public function generateSourceAndDestinationDiffSelectSQL(SourceInterface $source,
                                                              DestinationInterface $destination,
                                                              $intermediateTable = null,
                                                              $importHash = null)
    {
        $sourceTable = $intermediateTable ?: $source->getTable();
        $destinationTable = $destination->getTable();
        $sourceCodeColumn = $destination->getSourceCodeColumn();
        $columns = $destination->getColumns();
        $columnsToChar = $destination->getColumnsToChar();

        $this->functionCallingHelper->setImportHash($importHash);
        $functionName = $this->functionCreationHelper->generateFunctionName($destinationTable);
        $functionCalling = $this->functionCallingHelper->generateSQL($functionName, $sourceCodeColumn, $columns, $columnsToChar);

        $sql = '';
        $sql .= "SELECT $functionCalling AS operation" . PHP_EOL;
        $sql .= "FROM $sourceTable src" . PHP_EOL;
        $sql .= "FULL OUTER JOIN $destinationTable dest ON src.$sourceCodeColumn = dest.$sourceCodeColumn" . PHP_EOL;
        $sql .= ';';

        return $sql;
    }

    /**
     * @return string
     * @codeCoverageIgnore
     */
    public function generateSQLForImportRegTableCreation()
    {
        $sql = <<<'EOT'
CREATE TABLE IMPORT_REG (
	operation VARCHAR(128) NOT NULL,
	table_name VARCHAR(128) NOT NULL,
	source_code TEXT,
	field_name VARCHAR(128),
	from_value TEXT,
	to_value TEXT,
	sql TEXT NOT NULL,
	created_on TIMESTAMP(0) WITH TIME ZONE NOT NULL,
	executed_on TIMESTAMP(0) WITH TIME ZONE,
	import_hash VARCHAR(255) NOT NULL
);
EOT;
        return $sql;
    }

    /**
     * @param string $tableName
     * @return string
     * @codeCoverageIgnore
     */
    public function generateSQLForIntermmediateTableDrop($tableName)
    {
        return "DROP TABLE IF EXISTS $tableName CASCADE ;";
    }
}