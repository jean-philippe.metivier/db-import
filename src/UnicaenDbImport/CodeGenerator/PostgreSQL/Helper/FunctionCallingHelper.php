<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL\Helper;

use UnicaenDbImport\CodeGenerator\PostgreSQL\PostgreSQLCommonsTrait;

/**
 * Version PostgreSQL.
 *
 * @author Unicaen
 */
class FunctionCallingHelper extends \UnicaenDbImport\CodeGenerator\Common\Helper\FunctionCallingHelper
{
    use PostgreSQLCommonsTrait;
}