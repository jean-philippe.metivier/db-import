<?php

namespace UnicaenDbImportUnitTest\Domain;

use UnicaenDbImport\Domain\Destination;
use UnicaenDbImport\Domain\Import;
use UnicaenDbImport\Domain\Source;

class ImportTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getInvalidConfig
     * @expectedException \UnicaenDbImport\Config\ConfigException
     * @param array $config
     */
    public function test_can_validate_config(array $config)
    {
        Import::fromConfig($config);
    }

    public function getInvalidConfig()
    {
        $source = $this->createMock(Source::class);
        $destination = $this->createMock(Destination::class);

        return [
            [[]],
            [['source' => '']],
            [['destination' => '']],
            [['source' => 's', 'destination' => 12]],
            [['source' => $source, 'destination' => '']],
            [['source' => '', 'destination' => $destination]],
            [['source' => $source, 'destination' => $destination, 'intermediate_table' => '']],
            [['source' => $source, 'destination' => $destination, 'intermediate_table' => 12]],
        ];
    }

    public function test_can_map_config_keys()
    {
        $source = $this->createMock(Source::class);
        $destination = $this->createMock(Destination::class);

        $config = [
            'name'               => 'lorem ipsum',
            'source'             => $source,
            'destination'        => $destination,
            'intermediate_table' => 'src_ztemptable',
        ];

        $import = Import::fromConfig($config);

        $this->assertEquals('lorem ipsum', $import->getName());
        $this->assertEquals('lorem ipsum', $import->__toString());
        $this->assertEquals($source, $import->getSource());
        $this->assertEquals($destination, $import->getDestination());
        $this->assertEquals('src_ztemptable', $import->getIntermediateTable());
    }

    public function test_can_produce_default_name()
    {
        $source = $this->createMock(Source::class);
        $destination = $this->createMock(Destination::class);

        $config = [
            // no name !
            'source'      => $source,
            'destination' => $destination,
        ];

        $import = Import::fromConfig($config);

        $this->assertNotEmpty($import->getName());
    }

    public function test_can_produce_default_intermediate_table()
    {
        $source = $this->createMock(Source::class);
        $destination = $this->createMock(Destination::class);

        $config = [
            'source'      => $source,
            'destination' => $destination,
            // no 'intermediate_table'
        ];

        $import = Import::fromConfig($config);

        $this->assertNotEmpty($import->getIntermediateTable());
    }

    public function test_can_require_using_intermediate_table_when_connections_are_different()
    {
        $source = $this->createMock(Source::class);
        $destination = $this->createMock(Destination::class);

        // different connections
        $source->expects($this->once())->method('getConnection')->willReturn('a');
        $destination->expects($this->once())->method('getConnection')->willReturn('b');

        $config = [
            'source'      => $source,
            'destination' => $destination,
        ];

        $import = Import::fromConfig($config);

        $this->assertTrue($import->requiresIntermediateTable());
    }
}