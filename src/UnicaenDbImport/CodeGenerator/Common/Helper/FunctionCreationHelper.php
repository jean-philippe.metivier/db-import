<?php

namespace UnicaenDbImport\CodeGenerator\Common\Helper;

/**
 * Génération de code SQL de création de la fonction "create_import_metarequest_for_{table}".
 *
 * Version commune à toutes les plateformes de bases de données.
 *
 * @author Unicaen
 */
abstract class FunctionCreationHelper extends AbstractHelper
{
    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array  $columns
     * @return string
     */
    public function generateSQL($destinationTable, $sourceCodeColumn, array $columns)
    {
        $res = '';
        $res .= $this->generateFunctionCreationSQLSnippet($destinationTable, $sourceCodeColumn, $columns) . PHP_EOL;
        $res .= $this->generateFunctionBodyBeginningSQLSnippet() . PHP_EOL;
        $res .= $this->indent(4, $this->generateFunctionArgsNormalizationSQLSnippet($columns)) . PHP_EOL;
        $res .= $this->indent(4, $this->generateFunctionBodyContentSQLSnippet($destinationTable, $sourceCodeColumn, $columns)) . PHP_EOL;
        $res .= $this->generateFunctionBodyEndingSQLSnippet();

        return $res;
    }

    /**
     * @param string $destinationTable
     * @return string
     */
    public function generateFunctionName($destinationTable)
    {
        return 'create_import_metarequest_for_' . $destinationTable;
    }


    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array  $columns
     * @return string
     */
    protected function generateFunctionCreationSQLSnippet($destinationTable, $sourceCodeColumn, array $columns)
    {
        $varchar = $this->getPlatform()->getVarcharTypeDeclarationSQL([]);

        $res = '';
        $res .= 'CREATE OR REPLACE FUNCTION ' . $this->generateFunctionName($destinationTable) . '(' . PHP_EOL;
        $res .= $this->indent(4, $this->generateSignatureArgsList($sourceCodeColumn, $columns)) . PHP_EOL;
        $res .= ') RETURNS ' . $varchar . ' AS';

        return $res;
    }

    /**
     * @param string $sourceCodeColumn
     * @param array  $columns
     * @return string
     */
    protected function generateSignatureArgsList($sourceCodeColumn, array $columns)
    {
        $clob = $this->getPlatform()->getClobTypeDeclarationSQL([]);
        $varchar = $this->getPlatform()->getVarcharTypeDeclarationSQL([]);
        $timestamp = $this->getPlatform()->getDateTimeTzTypeDeclarationSQL([]);

        $sourceCols = array_merge([$sourceCodeColumn], $columns);
        $destinCols = array_merge([$sourceCodeColumn], $columns);

        $sourceColsWithTypes = array_map(function ($col) use ($clob) {
            return 'src_' . $col . ' ' . $clob;
        }, $sourceCols);
        $destinColsWithTypes = array_map(function ($col) use ($clob) {
            return 'dest_' . $col . ' ' . $clob;
        }, $destinCols);


        $res = '';
        $res .= implode(', ', $sourceColsWithTypes) . ',' . PHP_EOL;
        $res .= implode(', ', $destinColsWithTypes) . ', dest_deleted_on ' . $timestamp . ',' . PHP_EOL;
        $res .= 'import_hash ' . $varchar;

        return $res;
    }

    /**
     * @param array $columns
     * @return string
     */
    protected function generateFunctionArgsNormalizationSQLSnippet(array $columns)
    {
        $sourceCols = array_map(function ($col) {
            return 'src_' . $col;
        }, $columns);
        $destinCols = array_map(function ($col) {
            return 'dest_' . $col;
        }, $columns);

        $coalescer = function ($col) {
            return sprintf("%s = coalesce(%s, 'NULL');", $col, $this->getQuoteLiteralFunctionCallSQLSnippet($col));
        };

        $coalesceSourceColsSQLSnippet = array_map($coalescer, $sourceCols);
        $coalesceDestinColsSQLSnippet = array_map($coalescer, $destinCols);

        return
            "-- normalisation des valeurs d'entrée" . PHP_EOL .
            implode(PHP_EOL, $coalesceSourceColsSQLSnippet) . PHP_EOL .
            implode(PHP_EOL, $coalesceDestinColsSQLSnippet) . PHP_EOL;
    }

    /**
     * @return string
     */
    abstract protected function generateFunctionBodyBeginningSQLSnippet();

    /**
     * @return string
     */
    abstract protected function generateFunctionBodyEndingSQLSnippet();

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array  $columns
     * @return string
     */
    protected function generateFunctionBodyContentSQLSnippet($destinationTable, $sourceCodeColumn, array $columns)
    {
        $res = '';
        $res .= $this->generateInsertOperationMarkup($destinationTable, $sourceCodeColumn, $columns)   . PHP_EOL . PHP_EOL;
        $res .= $this->generateUpdateOperationMarkup($destinationTable, $sourceCodeColumn, $columns)   . PHP_EOL . PHP_EOL;
        $res .= $this->generateUndeleteOperationMarkup($destinationTable, $sourceCodeColumn, $columns) . PHP_EOL . PHP_EOL;
        $res .= $this->generateDeleteOperationMarkup($destinationTable, $sourceCodeColumn)   . PHP_EOL . PHP_EOL;
        $res .= "RETURN operation;";

        return $res;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array  $columns
     * @return string
     */
    protected function generateInsertOperationMarkup($destinationTable, $sourceCodeColumn, array $columns)
    {
        $hashConcatExpr = $this->getPlatform()->getConcatExpression("src_$sourceCodeColumn", "'-'", 'import_hash');

        $insertOperationTemplate = <<<EOT
-- l'enregistrement existe dans la source mais pas dans la destination : il devra être ajouté
IF (src_$sourceCodeColumn IS NOT NULL AND dest_$sourceCodeColumn IS NULL) THEN
    operation = 'insert';
    hash = $hashConcatExpr;
%s
END IF;
EOT;
        $plsql = '';

        $quotedSrcSourceCodeColumn = $this->getQuoteLiteralFunctionCallSQLSnippet("src_$sourceCodeColumn");
        $now = $this->getPlatform()->getNowExpression();

        /**
         * PL/SQL construisant l'instruction SQL qui sera inscrite dans le registre d'import.
         */
        $cols = array_merge([$sourceCodeColumn], $columns);
        $sourceCols = array_map(function ($col) {
            return 'src_' . $col;
        }, $columns);
        // instruction de mise à jour de la table destination
        $concatArgs = [
            "'INSERT INTO $destinationTable(" . implode(', ', $cols) . ", created_on) VALUES ('",
            $quotedSrcSourceCodeColumn,
            "', '"
        ];
        foreach ($sourceCols as $col) {
            $concatArgs[] = $col;
            $concatArgs[] = "', '";
        }
        $concatArgs[] = "'$now) ;'";
        $plsql .= "sql = " . $this->getPlatform()->getConcatExpression(...$concatArgs) . " ;" . PHP_EOL;
        // suivie de l'instruction permettant de marquer comme exécutée la ligne du registre d'import
        $plsql .= $this->generateRegisterExecutionDateUpdateConcat() . PHP_EOL;

        /**
         * PL/SQL inscrivant l'opération dans le registre d'import.
         */
        $plsql .= $this->generateInsertIntoImportRegisterTableForInsertOperation($destinationTable, $sourceCodeColumn);


        $markup = sprintf($insertOperationTemplate,
            $this->indent(4, $plsql)
        );

        return $markup;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array  $columns
     * @return string
     */
    protected function generateUpdateOperationMarkup($destinationTable, $sourceCodeColumn, array $columns)
    {
        $updateOperationMarkup = '';
        foreach ($columns as $col) {
            $hashConcatExpr = $this->getPlatform()->getConcatExpression("dest_$sourceCodeColumn", "'-'", "dest_$col", "'-'", 'import_hash');

            $template = <<<EOT
-- '$col' doit être mis à jour
IF (src_$col <> dest_$col) THEN
    operation = 'update';
    hash = $hashConcatExpr;
%s
END IF;
EOT;
            $srcCol = "src_$col";
            $quotedDestSourceCodeColumn = $this->getQuoteLiteralFunctionCallSQLSnippet("dest_$sourceCodeColumn");
            $now = $this->getPlatform()->getNowExpression();

            $plsql = '';

            /**
             * PL/SQL construisant l'instruction SQL qui sera inscrite dans le registre d'import.
             */
            // instruction de mise à jour de la table destination
            $plsql .= "sql = " . $this->getPlatform()->getConcatExpression(
                "'UPDATE $destinationTable SET $col = '",
                $srcCol,
                "', updated_on = $now WHERE $sourceCodeColumn = '",
                $quotedDestSourceCodeColumn,
                "' ;'"
            ) . " ;" . PHP_EOL;

            // suivie de l'instruction permettant de marquer comme exécutée la ligne du registre d'import
            $plsql .= $this->generateRegisterExecutionDateUpdateConcat() . PHP_EOL;

            /**
             * PL/SQL inscrivant l'opération dans le registre d'import.
             */
            $plsql .= $this->generateInsertIntoImportRegisterTableForUpdateOperation($destinationTable, $sourceCodeColumn, $col);


            $updateOperationMarkup .= sprintf($template,
                $this->indent(4, $plsql)
            );
            $updateOperationMarkup .= PHP_EOL;
        }

        $template = <<<EOT
-- l'enregistrement existe dans la destination et n'est pas historisé
IF (src_$sourceCodeColumn IS NOT NULL AND dest_$sourceCodeColumn IS NOT NULL and dest_deleted_on IS NULL) THEN
%s
END IF;
EOT;
        $markup = sprintf($template,
            $this->indent(4, $updateOperationMarkup)
        );

        return $markup;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param array  $columns
     * @return string
     */
    protected function generateUndeleteOperationMarkup($destinationTable, $sourceCodeColumn, array $columns)
    {
        $hashConcatExpr = $this->getPlatform()->getConcatExpression("dest_$sourceCodeColumn", "'-'", 'import_hash');

        $template = <<<EOT
-- l'enregistrement existe dans la destination mais historisé : il sera dé-historisé
IF (src_$sourceCodeColumn IS NOT NULL AND dest_$sourceCodeColumn IS NOT NULL and dest_deleted_on IS NOT NULL) THEN
    operation = 'undelete';
    hash = $hashConcatExpr;
%s
END IF;
EOT;
        $quotedDestSourceCodeColumn = $this->getQuoteLiteralFunctionCallSQLSnippet("dest_$sourceCodeColumn");
        $now = $this->getPlatform()->getNowExpression();

        $plsql = '';

        /**
         * PL/SQL construisant l'instruction SQL qui sera inscrite dans le registre d'import.
         */
        // instruction de mise à jour de la table destination
        $concatArgs = [
            "'UPDATE $destinationTable SET '",
        ];
        foreach ($columns as $col) {
            $concatArgs[] = "'$col = '";
            $concatArgs[] = "src_$col";
            $concatArgs[] = "', '";
        }
        $concatArgs[] = "'updated_on = $now, deleted_on = null WHERE $sourceCodeColumn = '";
        $concatArgs[] = $quotedDestSourceCodeColumn;
        $concatArgs[] = "' ;'";
        $plsql .= "sql = " . $this->getPlatform()->getConcatExpression(...$concatArgs) . " ;" . PHP_EOL;
        // suivie de l'instruction permettant de marquer comme exécutée la ligne du registre d'import
        $plsql .= $this->generateRegisterExecutionDateUpdateConcat() . PHP_EOL;

        /**
         * PL/SQL inscrivant l'opération dans le registre d'import.
         */
        $plsql .= $this->generateInsertIntoImportRegisterTableForUndeleteOperation($destinationTable, $sourceCodeColumn);

        $markup = sprintf($template,
            $this->indent(4, $plsql)
        );

        return $markup;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @return string
     */
    protected function generateDeleteOperationMarkup($destinationTable, $sourceCodeColumn) {

        $hashConcatExpr = $this->getPlatform()->getConcatExpression("dest_$sourceCodeColumn", "'-'", 'import_hash');

        $deleteOperationTemplate = <<<EOT
-- l'enregistrement existe dans la destination mais plus dans la source : il sera historisé
IF (src_$sourceCodeColumn IS NULL AND dest_$sourceCodeColumn IS NOT NULL and dest_deleted_on IS NULL) THEN
    operation = 'delete';
    hash = $hashConcatExpr;
%s
END IF;
EOT;
        $quotedDestSourceCodeColumn = $this->getQuoteLiteralFunctionCallSQLSnippet("dest_$sourceCodeColumn");
        $now = $this->getPlatform()->getNowExpression();

        $plsql = '';

        /**
         * PL/SQL construisant l'instruction SQL qui sera inscrite dans le registre d'import.
         */
        // instruction de mise à jour de la table destination
        $plsql .= "sql = " . $this->getPlatform()->getConcatExpression(
            "'UPDATE $destinationTable SET deleted_on = $now WHERE $sourceCodeColumn = '",
            $quotedDestSourceCodeColumn,
            "' ;'"
        ) . " ;" . PHP_EOL;
        // suivie de l'instruction permettant de marquer comme exécutée la ligne du registre d'import
        $plsql .= $this->generateRegisterExecutionDateUpdateConcat() . PHP_EOL;

        /**
         * PL/SQL inscrivant l'opération dans le registre d'import.
         */
        $plsql .= $this->generateInsertIntoImportRegisterTableForDeleteOperation($destinationTable, $sourceCodeColumn);

        $markup = sprintf($deleteOperationTemplate,
            $this->indent(4, $plsql)
        );

        return $markup;
    }

    /**
     * @return string
     */
    protected function generateRegisterExecutionDateUpdateConcat()
    {
        $quotedHashExpr = $this->getQuoteLiteralFunctionCallSQLSnippet("hash");
        $now = $this->getPlatform()->getNowExpression();

        $concatExpr = $this->getPlatform()->getConcatExpression(
            "sql",
            "' UPDATE import_reg SET executed_on = $now WHERE import_hash = '",
            $quotedHashExpr,
            "' ;'"
        );

        return "sql = " . $concatExpr . " ;";
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @return string
     */
    protected function generateInsertIntoImportRegisterTableForInsertOperation($destinationTable, $sourceCodeColumn)
    {
        $now = $this->getPlatform()->getNowExpression();

        $res =
            "INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) " .
            "VALUES ('insert', '$destinationTable', src_$sourceCodeColumn, null, null, null, sql, $now, hash);";

        return $res;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @param $field
     * @return string
     */
    protected function generateInsertIntoImportRegisterTableForUpdateOperation($destinationTable, $sourceCodeColumn, $field)
    {
        $now = $this->getPlatform()->getNowExpression();

        $res =
            "INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) " .
            "VALUES ('update', '$destinationTable', src_$sourceCodeColumn, '$field', src_$field, dest_$field, sql, $now, hash);";

        return $res;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @return string
     */
    protected function generateInsertIntoImportRegisterTableForUndeleteOperation($destinationTable, $sourceCodeColumn)
    {
        $now = $this->getPlatform()->getNowExpression();

        $res =
            "INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) " .
            "VALUES ('undelete', '$destinationTable', src_$sourceCodeColumn, null, null, null, sql, $now, hash);";

        return $res;
    }

    /**
     * @param string $destinationTable
     * @param string $sourceCodeColumn
     * @return string
     */
    protected function generateInsertIntoImportRegisterTableForDeleteOperation($destinationTable, $sourceCodeColumn)
    {
        $now = $this->getPlatform()->getNowExpression();

        $res =
            "INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) " .
            "VALUES ('delete', '$destinationTable', src_$sourceCodeColumn, null, null, null, sql, $now, hash);";

        return $res;
    }
}