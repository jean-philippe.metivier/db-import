<?php

namespace UnicaenDbImport\Domain;

interface ImportInterface
{
    /**
     * Retourne le petit nom de cet import.
     *
     * @return string
     */
    public function getName();

    /**
     * Retourne l'instance de la source de cet import.
     *
     * @return SourceInterface
     */
    public function getSource();

    /**
     * Retourne l'instance de la destination de cet import.
     *
     * @return \UnicaenDbImport\Domain\DestinationInterface
     */
    public function getDestination();

    /**
     * Retourne le nom de la table intermédiaire à créer dans la base de données de destination
     * lorsque l'utilisation d'une table intermédiaire est nécessaire (cf {@link requiresIntermediateTable}).
     *
     * @return string
     */
    public function getIntermediateTable();

    /**
     * Indique si la suppression automatique de la table intermédiaire "src_" est activée.
     *
     * @return bool
     */
    public function getIntermediateTableAutoDrop();

    /**
     * Détermine si l'utilisation d'une table intermédiaire dans la base de données de destination
     * est nécessaire.
     *
     * @return bool
     */
    public function requiresIntermediateTable();
}