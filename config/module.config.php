<?php

namespace UnicaenDbImport;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use UnicaenDbImport\Config\ImportConfig;
use UnicaenDbImport\Config\ImportConfigFactory;
use UnicaenDbImport\Controller\ConsoleControllerFactory;
use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLImporter;
use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLImporterFactory;
use UnicaenDbImport\Service\ImportService;
use UnicaenDbImport\Service\ImportServiceFactory;

return [
    'import' => [
        'importers' => [
            // database platform class => importer class
            PostgreSqlPlatform::class => PostgreSQLImporter::class,
        ],
        'imports' => [],
    ],

    'console' => [
        'router' => [
            'routes' => [
                'execute_imports' => [
                    'type'    => 'simple',
                    'options' => [
                        'route'    => 'run import [--all|-a] [--name=]',
                        'defaults' => [
                            'controller' => 'UnicaenDbImport\Controller\Console',
                            'action'     => 'runImport',
                        ],
                    ],
                ]
            ]
        ]
    ],

    'controllers' => [
        'factories' => [
            'UnicaenDbImport\Controller\Console' => ConsoleControllerFactory::class,
        ]
    ],

    'service_manager' => [
        'factories' => [
            ImportConfig::class => ImportConfigFactory::class,
            ImportService::class => ImportServiceFactory::class,

            PostgreSQLImporter::class => PostgreSQLImporterFactory::class,
        ],
    ],
];
