<?php

namespace UnicaenDbImport\Domain;

use Assert\AssertionChain;
use Doctrine\DBAL\Connection;
use UnicaenDbImport\Config\ConfigException;
use Zend\Stdlib\Parameters;

class Destination implements DestinationInterface
{
    protected $config;

    /**
     * @param array $config
     * @return static
     */
    static public function fromConfig(array $config)
    {
        $inst = new static($config);

        try {
            $inst->validateConfig();
        } catch (\Exception $e) {
            if (! $e instanceof ConfigException) {
                $e = new ConfigException($e->getMessage(), 0, $e);
            }
            throw $e;
        }

        return $inst;
    }

    protected function __construct(array $config)
    {
        $this->config = new Parameters($config);
    }

    protected function validateConfig()
    {
        (new AssertionChain($this->config->get($key = 'name'), "Une string non vide est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->string();

        if ($table = $this->config->get($key = 'table')) {
            (new AssertionChain($table, "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();
        }

        if ($this->config->get($key = 'select')) {
            throw ConfigException::illegalKey('select');
        }

        (new AssertionChain($this->config->get($key = 'connection'), "Une instance de " . Connection::class . " est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->isInstanceOf(Connection::class);

        (new AssertionChain($this->config->get($key = 'source_code_column'), "Une string non vide est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->string();

        (new AssertionChain($this->config->get($key = 'columns'), "Un tableau de strings non vides est requis pour la clé suivante: $key"))
            ->isArray()
            ->notEmpty()
            ->all()
            ->notEmpty()
            ->string();

        if ($this->config->get($key = 'columns_to_char')) {
            (new AssertionChain($this->config->get($key = 'columns_to_char'), "Un tableau de string => string non vides est requis pour la clé suivante: $key"))
                ->isArray()
                ->all()
                ->notEmpty()
                ->string();
        }
    }

    public function getName()
    {
        return $this->config->get('name');
    }

    public function getTable()
    {
        return $this->config->get('table');
    }

    public function getConnection()
    {
        return $this->config->get('connection');
    }

    public function getSourceCodeColumn()
    {
        return $this->config->get('source_code_column');
    }

    public function getColumns()
    {
        return $this->config->get('columns');
    }

    public function getColumnsToChar()
    {
        return $this->config->get('columns_to_char') ?: [];
    }
}