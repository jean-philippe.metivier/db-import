<?php

namespace UnicaenDbImport;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Statement;

/**
 * @codeCoverageIgnore
 *
 * @author Unicaen
 */
class QueryExecutor
{
    /**
     * @param string     $sql
     * @param Connection $connection
     * @return Statement
     */
    private function executeQuery($sql, Connection $connection)
    {
        return $connection->executeQuery($sql);
    }

    /**
     * @param string     $sql
     * @param Connection $connection
     * @return array
     */
    public function fetchAll($sql, Connection $connection)
    {
        $statement = $this->executeQuery($sql, $connection);

        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param string     $sql
     * @param Connection $connection
     * @return int
     */
    public function exec($sql, Connection $connection)
    {
        return $connection->exec($sql);
    }
}