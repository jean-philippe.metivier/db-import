<?php

namespace UnicaenDbImport\Service;

use UnicaenDbImport\Config\ImportConfig;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImportServiceFactory
{
    public function __invoke(ServiceLocatorInterface $sl)
    {
        /** @var ImportConfig $config */
        $config = $sl->get(ImportConfig::class);

        $service = new ImportService($config);

        return $service;
    }
}