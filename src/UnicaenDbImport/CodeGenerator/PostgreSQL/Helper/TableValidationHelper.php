<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL\Helper;

use UnicaenDbImport\CodeGenerator\PostgreSQL\PostgreSQLCommonsTrait;

/**
 * Version PostgreSQL.
 *
 * @author Unicaen
 */
class TableValidationHelper extends \UnicaenDbImport\CodeGenerator\Common\Helper\TableValidationHelper
{
    use PostgreSQLCommonsTrait;

    public function generateSQLForTableExistenceCheck($tableName)
    {
        $sql = <<<EOT
SELECT count(*) as table_exists
FROM   information_schema.tables
WHERE  table_schema = 'public' AND table_name = '$tableName'
;
EOT;
        return $sql;
    }

    public function generateSQLForColumnsValidation($tableName, array $columnsAndTypes)
    {
        $selects = [];
        foreach ($columnsAndTypes as $column => $type) {
            if (is_numeric($column)) {
                $column = $type;
                $type = 'NULL';
            } else {
                $type = "'$type'";
            }
            $selects[] = "SELECT '$column', $type";
        }
        $selects = implode(' UNION' . PHP_EOL, $selects);

        $sql = <<<EOT

WITH required_cols(column_name, column_type) AS (
$selects
)
SELECT rc.column_name, c.udt_name
FROM required_cols rc
JOIN information_schema.columns c ON upper(c.column_name) = upper(rc.column_name)
WHERE c.table_name = '$tableName' AND (rc.column_type  IS NULL OR rc.column_type = c.udt_name)
;
EOT;
        return $sql;
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForHistoColumnsValidation($tableName)
    {
        $udtName = 'timestamptz';

        $columnsAndTypes = [
            'created_on' => $udtName,
            'updated_on' => $udtName,
            'deleted_on' => $udtName,
        ];

        return $this->generateSQLForColumnsValidation($tableName, $columnsAndTypes);
    }
}