<?php

namespace UnicaenDbImport\Controller;

use Interop\Container\ContainerInterface;
use UnicaenDbImport\Service\ImportService;

class ConsoleControllerFactory
{
    function __invoke(ContainerInterface $sl)
    {
        /** @var ImportService $importService */
        $importService = $sl->get(ImportService::class);

        $controller = new ConsoleController();
        $controller->setImportService($importService);

        return $controller;
    }
}