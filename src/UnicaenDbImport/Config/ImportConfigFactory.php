<?php

namespace UnicaenDbImport\Config;

use Doctrine\DBAL\Connection;
use UnicaenDbImport\Domain\Destination;
use UnicaenDbImport\Domain\Import;
use UnicaenDbImport\Domain\Source;
use UnicaenDbImport\Config\ConfigException;
use UnicaenDbImport\Importer\ImporterException;
use UnicaenDbImport\Domain\Exception\ImportNotFoundException;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImportConfigFactory
{
    const CONFIG_KEY = 'import';

    /**
     * @var ServiceLocatorInterface
     */
    private $sl;

    public function __invoke(ServiceLocatorInterface $sl)
    {
        $this->sl = $sl;

        $appConfig = $sl->get('Config');

        if (!array_key_exists($key = self::CONFIG_KEY, $appConfig)) {
            throw ConfigException::missingKey($key);
        }

        $config = $appConfig[$key];

        $importers = $this->createImporters($config);
        $imports = $this->createImports($config);

        $importConfig = new ImportConfig();
        $importConfig->setImporters($importers);
        $importConfig->setImports($imports);

        return $importConfig;
    }

    private function createImporters($config)
    {
        if (!array_key_exists($key = 'importers', $config)) {
            throw ConfigException::missingKey($key);
        }

        $importers = [];
        if (array_key_exists($key = 'importers', $config)) {
            foreach ($config[$key] as $databasePlatformClass => $importerClass) {
                $importers[$databasePlatformClass] = $this->createImporter($importerClass);
            }
        }

        return $importers;
    }

    private function createImporter($importerClass)
    {
        if (!$this->sl->has($importerClass)) {
            throw ImporterException::importerServiceNotFound(sprintf("Le service locator n'a pas trouvé l'Importer spécifié par la clé '%s'", $importerClass));
        }

        $importer = $this->sl->get($importerClass);

//        if ($importer instanceof CodeGeneratorAwareInterface && array_key_exists($key = 'code_generator', $importerOptions)) {
//            $codeGeneratorClass = $importerOptions[$key];
//            $codeGenerator = $this->sl->has($codeGeneratorClass) ? $this->sl->get($codeGeneratorClass) : new $codeGeneratorClass;
//            $importer->setCodeGenerator($codeGenerator);
//        }

        return $importer;
    }

    private function createImports($config)
    {
        $imports = [];
        if (array_key_exists($key = 'imports', $config)) {
            foreach ($config[$key] as $importArr) {
                $imports[] = $this->createImport($importArr);
            }
        }

        return $imports;
    }

    private function createImport($importConfig)
    {
        if (!array_key_exists($key = 'source', $importConfig)) {
            throw ConfigException::missingKey($key);
        }
        if (!array_key_exists($key = 'destination', $importConfig)) {
            throw ConfigException::missingKey($key);
        }

        $source      = $this->createSource($importConfig['source']);
        $destination = $this->createDestination($importConfig['destination']);

        $importConfig['source'] = $source;
        $importConfig['destination'] = $destination;

        return Import::fromConfig($importConfig);
    }

    private function createSource(array $config)
    {
        /** @var Connection $conn */
        $conn = $this->sl->get($config['connection']);
        $config['connection'] = $conn;

        $source = Source::fromConfig($config);

        return $source;
    }

    private function createDestination(array $config)
    {
        /** @var Connection $conn */
        $conn = $this->sl->get($config['connection']);
        $config['connection'] = $conn;

        return Destination::fromConfig($config);
    }
}