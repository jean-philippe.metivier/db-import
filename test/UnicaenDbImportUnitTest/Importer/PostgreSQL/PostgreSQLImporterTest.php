<?php

namespace UnicaenDbImportUnitTest\Importer\PostgreSQL;

use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLImporter;
use UnicaenDbImportUnitTest\Importer\ImporterAbstractTest;

class PostgreSQLImporterTest extends ImporterAbstractTest
{
    protected function setUp()
    {
        parent::setUp();

        $this->importer = new PostgreSQLImporter($this->databaseFacade);
    }
}