<?php

namespace UnicaenDbImport\Domain;

class ImportResult
{
    private $importName;
    private $resultsByOperation = [];
    private $sql = '';

    /**
     * @var \Exception
     */
    private $exception;

    /**
     * @param $array
     * @return static
     */
    public static function fromImportRegisterFetchResult($array)
    {
        $inst = new static();

        $dataByOperation = [];
        foreach ($array as $row) {
            $operation = $row['operation'];

            if (!array_key_exists($operation, $dataByOperation)) {
                $dataByOperation[$operation] = [];
            }

            // sql
            if (!array_key_exists('sql', $dataByOperation[$operation])) {
                $dataByOperation[$operation]['sql'] = '';
            }
            $dataByOperation[$operation]['sql'] .= $row['sql'] . PHP_EOL;

            // count
            if (!array_key_exists('count', $dataByOperation[$operation])) {
                $dataByOperation[$operation]['count'] = 0;
            }
            $dataByOperation[$operation]['count'] += 1;
        }

        foreach ($dataByOperation as $operation => $data) {
            $inst->setResultsByOperation($data, $operation);
        }

        return $inst;
    }

    /**
     * @param $array
     * @return static
     * @deprecated
     */
    public static function fromDiffRequestResult($array)
    {
        $inst = new static();

        foreach ($array as $key => $data) {
            if ($key === 'sql') {
                continue;
            }
            $inst->setResultsByOperation($data, $key);
        }

        return $inst;
    }

    /**
     * @param string $importName
     * @return static
     */
    public function setImportName($importName)
    {
        $this->importName = $importName;

        return $this;
    }

    /**
     * @return string
     */
    public function getImportName()
    {
        return $this->importName;
    }

    /**
     * @param array $data
     * @param string $operation
     * @return static
     */
    public function setResultsByOperation(array $data, $operation)
    {
        $this->resultsByOperation[$operation] = $data;

        $this->sql .= $data['sql'] . PHP_EOL;

        return $this;
    }

    /**
     * @return string
     */
    public function getSQL()
    {
        return $this->sql;
    }

    /**
     * @param $operation
     * @return int
     */
    public function getInstructionCountForOperation($operation)
    {
        if (!array_key_exists($operation, $this->resultsByOperation)) {
            return 0;
        }

        return $this->resultsByOperation[$operation]['count'];
    }

    /**
     * @param $operation
     * @return string|null
     */
    public function getSQLForOperation($operation)
    {
        if (!array_key_exists($operation, $this->resultsByOperation)) {
            return null;
        }

        return $this->resultsByOperation[$operation]['sql'];
    }

    /**
     * @return \Exception
     */
    public function getException()
    {
        return $this->exception;
    }

    /**
     * @param \Exception $exception
     * @return ImportResult
     */
    public function setException(\Exception $exception = null)
    {
        $this->exception = $exception;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        if (count($this->resultsByOperation) === 0) {
            return "Aucune instruction exécutée.";
        }

        $str = '';
        foreach ($this->resultsByOperation as $operation => $results) {
            $str .= "# " . $operation . " :" . PHP_EOL . $results['sql'] . "(" . $results['count'] . " instructions exécutées)" . PHP_EOL;
        }

        return $str;
    }
}