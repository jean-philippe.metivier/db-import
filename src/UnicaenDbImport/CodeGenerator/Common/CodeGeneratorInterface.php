<?php

namespace UnicaenDbImport\CodeGenerator\Common;

use UnicaenApp\Exception\RuntimeException;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\SourceInterface;

/**
 * Contrat à honorer pour les générateurs de code SQL.
 *
 * @package UnicaenDbImport\CodeGenerator\Common
 */
interface CodeGeneratorInterface
{
    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForTableExistenceCheck($tableName);

    /**
     * @param array $result
     * @return bool
     */
    public function convertTableExistenceCheckResultToBoolean(array $result);

    /**
     * @param string $tableName
     * @param array  $columns
     * @return string
     */
    public function generateSQLForDataColumnsValidation($tableName, array $columns);

    /**
     * @param string $tableName
     * @param array  $result
     * @param array  $expectedColumns
     * @return null|RuntimeException
     */
    public function convertDataColumnsValidationBadResultToException($tableName, array $result, array $expectedColumns);

    /**
     * @param string $tableName
     * @return mixed
     */
    public function generateSQLForHistoColumnsValidation($tableName);

    /**
     * @param string $tableName
     * @param array $result
     * @return null|RuntimeException
     */
    public function convertHistoColumnsValidationBadResultToException($tableName, array $result);

    /**
     * @param SourceInterface $source
     * @return string
     */
    public function generateSQLForSelectFromSource(SourceInterface $source);

    /**
     * @param SourceInterface      $source
     * @param DestinationInterface $destination
     * @param string               $intermediateTable
     * @return string
     */
    public function generateSQLForIntermediateTableCreation(SourceInterface $source,
                                                            DestinationInterface $destination,
                                                            $intermediateTable);

    /**
     * @param string $intermediateTable
     * @param array  $columnsValues
     * @return string
     */
    public function generateSQLForInsertIntoIntermmediateTable($intermediateTable, array $columnsValues);

    /**
     * @return string
     */
    public function generateSQLForImportRegTableCreation();

    /**
     * @param string $tableName
     * @return string
     */
    public function generateSQLForIntermmediateTableDrop($tableName);

    /**
     * @param \UnicaenDbImport\Domain\SourceInterface $source
     * @param DestinationInterface                    $destination
     * @return string
     */
    public function generateImportMetaRequestFunctionCreationSQL(SourceInterface $source,
                                                                 DestinationInterface $destination);

    /**
     * @param SourceInterface      $source
     * @param DestinationInterface $destination
     * @param string               $intermediateTable
     * @param string               $importHash
     * @return string
     */
    public function generateSourceAndDestinationDiffSelectSQL(SourceInterface $source,
                                                              DestinationInterface $destination,
                                                              $intermediateTable = null,
                                                              $importHash = null);
    /**
     * @return string
     */
    public function generateSQLForFetchingImportRequestsToExecute();
}