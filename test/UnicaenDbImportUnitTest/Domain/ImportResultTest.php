<?php

namespace UnicaenDbImportUnitTest\Domain;

use UnicaenDbImport\Domain\ImportResult;

class ImportResultTest extends \PHPUnit_Framework_TestCase
{
    public function test_can_instantiate_from_diff_request_result()
    {
        $arrayResult = [
            'update' => [
                'count' => 2,
                'sql' => 'UPDATE SQL',
            ],
            'insert' => [
                'count' => 1,
                'sql' => 'INSERT SQL',
            ],
        ];

        $result = ImportResult::fromDiffRequestResult($arrayResult);

        self::assertNull($result->getImportName());
        self::assertEquals(2, $result->getInstructionCountForOperation('update'));
        self::assertEquals(1, $result->getInstructionCountForOperation('insert'));
        self::assertEquals(0, $result->getInstructionCountForOperation('unexpected operation'));
        self::assertEquals('UPDATE SQL', $result->getSQLForOperation('update'));
        self::assertEquals('INSERT SQL', $result->getSQLForOperation('insert'));
        self::assertNull($result->getSQLForOperation('unexpected operation'));
        self::assertEquals(<<<EOT
UPDATE SQL
INSERT SQL

EOT
        , $result->getSQL());
        self::assertInternalType('string', $result->__toString());
    }
}