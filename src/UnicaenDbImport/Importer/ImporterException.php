<?php

namespace UnicaenDbImport\Importer;

use UnicaenApp\Exception\RuntimeException;

class ImporterException extends RuntimeException
{
    public static function importerServiceNotFound($importerClass)
    {
        return new static("Importer introuvable avec l'identifiant de service suivant: " . $importerClass);
    }
}