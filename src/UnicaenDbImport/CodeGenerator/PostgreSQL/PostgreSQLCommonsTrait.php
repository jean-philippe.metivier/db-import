<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;

trait PostgreSQLCommonsTrait
{
    protected $platform;

    /**
     * @return PostgreSqlPlatform
     */
    protected function getPlatform()
    {
        if (null === $this->platform) {
            $this->platform = new PostgreSqlPlatform();
        }

        return $this->platform;
    }

    /**
     * @param string $argument
     * @return string
     */
    protected function getQuoteLiteralFunctionCallSQLSnippet($argument)
    {
        return 'quote_literal(' . $argument . ')';
    }
}