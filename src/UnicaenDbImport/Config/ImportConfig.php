<?php

namespace UnicaenDbImport\Config;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use UnicaenApp\Exception\RuntimeException;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\Exception\ImportNotFoundException;
use UnicaenDbImport\Importer\ImporterInterface;

class ImportConfig
{
    /**
     * @var ImporterInterface[]
     */
    protected $importers = [];

    /**
     * @var ImportInterface[]
     */
    protected $imports = [];

    /**
     * @param \UnicaenDbImport\Domain\Import[] $imports
     * @return static
     */
    public function setImports(array $imports)
    {
        $this->imports = $imports;

        return $this;
    }

    /**
     * @return ImportInterface[]
     */
    public function getImports()
    {
        return $this->imports;
    }

    /**
     * @param string $name
     * @return ImportInterface
     */
    public function getImport($name)
    {
        foreach ($this->imports as $import) {
            if ($import->getName() === $name) {
                return $import;
            }
        }

        throw ImportNotFoundException::byName($name);
    }

    /**
     * @param ImporterInterface[] $importers
     * @return static
     */
    public function setImporters(array $importers)
    {
        $this->importers = $importers;

        return $this;
    }

    /**
     * @param AbstractPlatform $platform
     * @return ImporterInterface
     */
    public function getImporterForDatabasePlatform(AbstractPlatform $platform)
    {
        foreach ($this->importers as $databasePlatformClass => $importer) {
            if ($platform instanceof $databasePlatformClass) {
                return $importer;
            }
        }

        return null;
    }

    /**
     * @param \UnicaenDbImport\Domain\ImportInterface $import
     * @return ImporterInterface
     */
    public function getImporterForImport(ImportInterface $import)
    {
        $databasePlatform = $import->getDestination()->getConnection()->getDatabasePlatform();
        if ($databasePlatform === null) {
            throw new RuntimeException("Oups, aucune DatabasePlatform retournée par la connexion de la destination!");
        }

        $importer = $this->getImporterForDatabasePlatform($databasePlatform);
        if ($importer === null) {
            throw new RuntimeException("Impossible de fournir l'objet Importer pour l'import spécifié suivant: " . $import->getName());
        }

        return $importer;
    }
}