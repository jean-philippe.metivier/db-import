<?php

namespace UnicaenDbImport\Domain;

use Doctrine\DBAL\Connection;

interface SourceInterface
{
    /**
     * Retourne le petit nom de cette source.
     *
     * @return string
     */
    public function getName();

    /**
     * Retourne le nom de la table de source.
     *
     * @return string
     */
    public function getTable();

    /**
     * @return string
     */
    public function getSelect();

    /**
     * Retourne l'instance de la connexion Doctrine vers cette source.
     *
     * @return Connection
     */
    public function getConnection();

    /**
     * Retourne le nom de la colonne commune existant obligatoirement dans la table/vue source
     * ET la table destination.
     *
     * Dans les tables source et destination, cette colonne est celle d'un identifiant unique permettant
     * de rapprocher les enregistrements source et destination.
     *
     * @return string Exemple: 'code'
     */
    public function getSourceCodeColumn();

    /**
     * Retourne les noms des colonnes de la table/vue source concernées par l'import.
     *
     * Ne doit pas inclure la colonne commune retournée par {@link getSourceCodeColumn}.
     *
     * @return array Exemple: ['libelle', 'debut_validite', 'fin_validite']
     */
    public function getColumns();
}