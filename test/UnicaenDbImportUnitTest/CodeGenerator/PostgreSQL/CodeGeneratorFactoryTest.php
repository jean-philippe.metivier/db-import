<?php

namespace UnicaenDbImportUnitTest\CodeGenerator\PostgreSQL;

use UnicaenDbImport\CodeGenerator\PostgreSQL\CodeGenerator;
use UnicaenDbImport\CodeGenerator\PostgreSQL\CodeGeneratorFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class CodeGeneratorFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function test_can_create_service()
    {
        /** @var ServiceLocatorInterface|\PHPUnit_Framework_MockObject_MockObject $sl */
        $sl = $this->createMock(ServiceLocatorInterface::class);
        $sl->expects($this->never())->method('get');

        $factory = new CodeGeneratorFactory();
        $service = $factory->__invoke($sl);

        $this->assertInstanceOf(CodeGenerator::class, $service);
    }
}