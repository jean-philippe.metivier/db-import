<?php

namespace UnicaenDbImport\CodeGenerator\Common\Helper;

use UnicaenApp\Exception\RuntimeException;

/**
 * Génération de code SQL pour tester l'existence, la conformité de table.
 *
 * Version commune à toutes les plateformes de bases de données.
 *
 * @author Unicaen
 */
abstract class TableValidationHelper extends AbstractHelper
{
    /**
     * @param string $tableName
     * @return string
     */
    abstract public function generateSQLForTableExistenceCheck($tableName);

    /**
     * @param array $result
     * @return bool
     */
    public function convertTableExistenceCheckResultToBoolean(array $result)
    {
        return isset($result[0]['table_exists']) && $result[0]['table_exists'] === 1;
    }

    /**
     * @param string $tableName
     * @param array  $columnsAndTypes
     * @return string
     */
    abstract public function generateSQLForColumnsValidation($tableName, array $columnsAndTypes);

    /**
     * @param string $tableName
     * @param array  $result
     * @param array  $expectedColumns
     * @return null|RuntimeException
     */
    public function convertColumnsValidationResultToException($tableName, array $result, array $expectedColumns)
    {
        if (count($result) === count($expectedColumns)) {
            return null;
        }

        $message = "Assurez-vous que les colonnes suivantes existent bien dans la table '$tableName' : ";
        $message .= implode(', ', $expectedColumns);

        return new RuntimeException($message);
    }

    /**
     * @param string $tableName
     * @return string
     */
    abstract public function generateSQLForHistoColumnsValidation($tableName);

    /**
     * @param string $tableName
     * @param array $result
     * @return null|RuntimeException
     */
    public function convertHistoColumnsValidationBadResultToException($tableName, array $result)
    {
        if (count($result) === 3) {
            return null;
        }

        $timestamp = $this->getPlatform()->getDateTimeTzTypeDeclarationSQL([]);
        $now = $this->getPlatform()->getNowExpression();

        $message = "Assurez-vous que les colonnes d'historique suivantes sont présentes dans la table '$tableName' : " . PHP_EOL;
        $message .= <<<EOT
ALTER TABLE $tableName ADD created_on $timestamp DEFAULT $now NOT NULL ;
ALTER TABLE $tableName ADD updated_on $timestamp ;
ALTER TABLE $tableName ADD deleted_on $timestamp ;
EOT;

        return new RuntimeException($message);
    }
}