<?php

namespace UnicaenDbImportUnitTest\Domain;

use Doctrine\DBAL\Connection;
use UnicaenDbImport\Domain\Source;

class SourceTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider getInvalidConfig
     * @expectedException \UnicaenDbImport\Config\ConfigException
     * @param array $config
     */
    public function test_can_validate_config(array $config)
    {
        Source::fromConfig($config);
    }

    public function getInvalidConfig()
    {
        $connection = $this->createMock(Connection::class);

        return [
            [[]],
            [['name' => 'n']],
            [['name' => 12]],
            [['name' => 'n', 'table' => 't']],
            [['name' => 'n', 'table' => 12]],
            [['name' => 'n', 'table' => 't', 'select' => 's']], // 'select' xor 'table'
            [['name' => 'n', 'table' => 't', 'connection' => '']], // connection is not an instance of Connection
            [['name' => 'n', 'table' => 't', 'connection' => $connection]],
            [['name' => 'n', 'table' => 't', 'connection' => $connection, 'source_code_column' => '']],
            [['name' => 'n', 'table' => 't', 'connection' => $connection, 'source_code_column' => 's']],
            [['name' => 'n', 'table' => 't', 'connection' => $connection, 'source_code_column' => 's', 'columns' => []]],
            [['name' => 'n', 'table' => 't', 'connection' => $connection, 'source_code_column' => 's', 'columns' => ['']]],
            [['name' => 'n', 'table' => 't', 'connection' => $connection, 'source_code_column' => 's', 'columns' => ['c', 12]]],
        ];
    }

    public function test_can_map_config_keys()
    {
        $connection = $this->createMock(Connection::class);

        $config = [
            'name'               => 'n',
            'table'              => 't',
            'connection'         => $connection,
            'source_code_column' => 's',
            'columns'            => ['a', 'b'],
        ];

        $destination = Source::fromConfig($config);

        $this->assertEquals('n', $destination->getName());
        $this->assertEquals('t', $destination->getTable());
        $this->assertEquals($connection, $destination->getConnection());
        $this->assertEquals('s', $destination->getSourceCodeColumn());
        $this->assertEquals(['a', 'b'], $destination->getColumns());
    }
}