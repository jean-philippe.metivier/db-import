<?php

namespace UnicaenDbImport\Domain;

use Assert\AssertionChain;
use Doctrine\DBAL\Connection;
use UnicaenDbImport\Config\ConfigException;
use Zend\Stdlib\Parameters;

class Source implements SourceInterface
{
    protected $config;

    /**
     * @param array $config
     * @return static
     */
    static public function fromConfig(array $config)
    {
        $inst = new static($config);

        try {
            $inst->validateConfig();
        } catch (\Exception $e) {
            if (! $e instanceof ConfigException) {
                $e = new ConfigException($e->getMessage(), 0, $e);
            }
            throw $e;
        }

        return $inst;
    }

    protected function __construct(array $config)
    {
        $this->config = new Parameters($config);
    }

    protected function validateConfig()
    {
        (new AssertionChain($this->config->get($key = 'name'), "Une string non vide est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->string();

        if ($table = $this->config->get($key = 'table'))
            (new AssertionChain($table, "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();

        if ($select = $this->config->get($key = 'select'))
            (new AssertionChain($select, "Une string non vide est requise pour la clé suivante: $key"))
                ->notEmpty()
                ->string();

        if ($table && $select) {
            throw ConfigException::exclusiveKeys(['table', 'select']);
        }
        if (!$table && !$select) {
            throw ConfigException::atLeastOneKey(['table', 'select']);
        }

        (new AssertionChain($this->config->get($key = 'connection'), "Une instance de " . Connection::class . " est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->isInstanceOf(Connection::class);

        (new AssertionChain($this->config->get($key = 'source_code_column'), "Une string non vide est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->string();

        (new AssertionChain($this->config->get($key = 'columns'), "Un tableau de strings non vides est requis pour la clé suivante: $key"))
            ->isArray()
            ->notEmpty()
            ->all()
            ->notEmpty()
            ->string();
    }

    public function getName()
    {
        return $this->config->get('name');
    }

    public function getTable()
    {
        return $this->config->get('table');
    }

    public function getSelect()
    {
        return $this->config->get('select');
    }

    public function getConnection()
    {
        return $this->config->get('connection');
    }

    public function getSourceCodeColumn()
    {
        return $this->config->get('source_code_column');
    }

    public function getColumns()
    {
        return $this->config->get('columns');
    }
}