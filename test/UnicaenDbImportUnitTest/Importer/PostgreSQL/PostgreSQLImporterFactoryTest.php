<?php

namespace UnicaenDbImportUnitTest\Importer\PostgreSQL;

use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLImporter;
use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLImporterFactory;
use Zend\ServiceManager\ServiceLocatorInterface;

class PostgreSQLImporterFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function test_can_create_service()
    {
        /** @var ServiceLocatorInterface|\PHPUnit_Framework_MockObject_MockObject $sl */
        $sl = $this->createMock(ServiceLocatorInterface::class);
        $sl->expects($this->never())->method('get');

        $factory = new PostgreSQLImporterFactory();
        $service = $factory->__invoke($sl);

        $this->assertInstanceOf(PostgreSQLImporter::class, $service);
    }
}