<?php

namespace UnicaenDbImport\Importer\PostgreSQL;

use UnicaenDbImport\CodeGenerator\PostgreSQL\CodeGeneratorFactory;
use UnicaenDbImport\DatabaseFacade;
use UnicaenDbImport\QueryExecutor;
use Zend\ServiceManager\ServiceLocatorInterface;

class PostgreSQLImporterFactory
{
    public function __invoke(ServiceLocatorInterface $sl)
    {
        $codeGeneratorFactory = new CodeGeneratorFactory();
        $codeGenerator = $codeGeneratorFactory->__invoke($sl);

        $databaseFacade = new DatabaseFacade($codeGenerator, new QueryExecutor());

        $importer = new PostgreSQLImporter($databaseFacade);

        return $importer;
    }
}