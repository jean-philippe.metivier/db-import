<?php

namespace UnicaenDbImport\Importer;

use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\ImportResult;

interface ImporterInterface
{
    /**
     * @param ImportInterface $import
     * @return static
     */
    public function setImport(ImportInterface $import);

    /**
     * @return ImportResult Résultat de l'import
     */
    public function run();
}