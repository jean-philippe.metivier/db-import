<?php

namespace UnicaenDbImport\CodeGenerator\Common\Helper;

use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Classe mère des helpers de génération de code SQL.
 *
 * @author Unicaen
 */
abstract class AbstractHelper
{
    /**
     * @return AbstractPlatform
     */
    abstract protected function getPlatform();

    /**
     * @param string $argument
     * @return string
     */
    abstract protected function getQuoteLiteralFunctionCallSQLSnippet($argument);

    /**
     * @param int    $length
     * @param string $string
     * @return string
     */
    protected function indent($length, $string)
    {
        $prefix = str_repeat(' ', $length);

        $lines = explode(PHP_EOL, $string);
        $indentedLines = array_map(function($line) use ($prefix) {
            return strlen($line) > 0 ? $prefix . $line : '';
        }, $lines);

        return implode(PHP_EOL, $indentedLines);
    }
}