<?php

namespace UnicaenDbImportUnitTest\Importer;

use UnicaenDbImport\DatabaseFacade;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\ImportResult;
use UnicaenDbImport\Domain\SourceInterface;
use UnicaenDbImport\Importer\AbstractImporter;

abstract class ImporterAbstractTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AbstractImporter
     */
    protected $importer;

    /**
     * @var DatabaseFacade|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $databaseFacade;

    protected function setUp()
    {
        parent::setUp();

        $this->databaseFacade = $this->createMock(DatabaseFacade::class);
    }

    public function test_can_run_import_without_intermediate_table()
    {
        $import = $this->createImportMock();
        $import->expects($this->atLeastOnce())->method('requiresIntermediateTable')->willReturn(FALSE); // no intermediate table
        $import->expects($this->never())->method('getIntermediateTable');

        $this->importer->setImport($import);
        $result = $this->importer->run();

        self::assertInstanceOf(ImportResult::class, $result);
        $this->assertResultIsCorrect($result);
    }

    public function test_can_run_import_with_intermediate_table()
    {
        $import = $this->createImportMock();
        $import->expects($this->atLeastOnce())->method('requiresIntermediateTable')->willReturn(TRUE); // intermediate table
        $import->expects($this->once())->method('getIntermediateTable')->willReturn('interm_ztemptable');
        $this->databaseFacade->expects($this->once())->method('createIntermediateTable')->with('interm_ztemptable');
        $this->databaseFacade->expects($this->once())->method('populateIntermediateTable')->with('interm_ztemptable');

        $this->importer->setImport($import);
        $result = $this->importer->run();

        $this->assertInstanceOf(ImportResult::class, $result);
        $this->assertResultIsCorrect($result);
    }

    private function createImportMock()
    {
        /** @var SourceInterface|\PHPUnit_Framework_MockObject_MockObject $source */
        $source = $this->createMock(SourceInterface::class);

        /** @var DestinationInterface|\PHPUnit_Framework_MockObject_MockObject $destination */
        $destination = $this->createMock(DestinationInterface::class);
        $destination->expects($this->never())->method('getTable');

        /** @var ImportInterface|\PHPUnit_Framework_MockObject_MockObject $import */
        $import = $this->createMock(ImportInterface::class);
        $import->expects($this->once())->method('getName')->willReturn('import name');
        $import->expects($this->once())->method('getSource')->willReturn($source);
        $import->expects($this->once())->method('getDestination')->willReturn($destination);

        $this->databaseFacade->expects($this->once())->method('createMetaRequestCreationFunction')->with($source, $destination);
        $this->databaseFacade->expects($this->once())->method('executeDiffRequest');
        $this->databaseFacade->expects($this->once())->method('fetchImportRegister')->willReturn([
            ['operation' => 'update', 'sql' => 'SQL UPDATE 1'],
            ['operation' => 'update', 'sql' => 'SQL UPDATE 2'],
            ['operation' => 'insert', 'sql' => 'SQL INSERT 1'],
        ]);

        return $import;
    }

    private function assertResultIsCorrect(ImportResult $result)
    {
        $updateSQL = <<<EOT
SQL UPDATE 1
SQL UPDATE 2

EOT;
        $insertSQL = <<<EOT
SQL INSERT 1

EOT;
        self::assertEquals('import name', $result->getImportName());

        self::assertEquals(2, $result->getInstructionCountForOperation('update'));
        self::assertEquals(1, $result->getInstructionCountForOperation('insert'));

        self::assertEquals($updateSQL, $result->getSQLForOperation('update'));
        self::assertEquals($insertSQL, $result->getSQLForOperation('insert'));
    }

    public function test_run_result_contains_thrown_exception()
    {
        $destination = $this->createMock(DestinationInterface::class);

        /** @var ImportInterface|\PHPUnit_Framework_MockObject_MockObject $import */
        $import = $this->createMock(ImportInterface::class);
        $import->expects($this->once())->method('getName')->willReturn('import name');
        $import->expects($this->once())->method('getDestination')->willReturn($destination);

        $exception = new \Exception();
        $this->databaseFacade->expects($this->once())->method('validateDestinationTable')->willThrowException($exception);

        $this->importer->setImport($import);
        $result = $this->importer->run();

        self::assertInstanceOf(ImportResult::class, $result);
        self::assertSame($exception, $result->getException());
        self::assertEquals('import name', $result->getImportName());
    }
}