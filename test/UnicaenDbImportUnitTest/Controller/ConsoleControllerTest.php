<?php

namespace UnicaenDbImportUnitTest\Controller;

use UnicaenDbImport\Controller\ConsoleController;
use UnicaenDbImport\Controller\ConsoleControllerFactory;
use UnicaenDbImport\Service\ImportService;
use Zend\Mvc\Controller\ControllerManager;
use Zend\ServiceManager\ServiceLocatorInterface;

class ConsoleControllerTest extends \PHPUnit_Framework_TestCase
{
    public function test_can_create_controller()
    {
        $importService = $this->createMock(ImportService::class);

        /** @var ServiceLocatorInterface|\PHPUnit_Framework_MockObject_MockObject $sl */
        $sl = $this->createMock(ServiceLocatorInterface::class);
        $sl->expects($this->once())->method('get')->with(ImportService::class)->willReturn($importService);

        /** @var ControllerManager|\PHPUnit_Framework_MockObject_MockObject $cm */
        $cm = $this->createMock(ControllerManager::class);
        $cm->expects($this->once())->method('getServiceLocator')->willReturn($sl);

        $factory = new ConsoleControllerFactory();
        $controller = $factory->__invoke($cm);

        $this->assertInstanceOf(ConsoleController::class, $controller);
    }


}