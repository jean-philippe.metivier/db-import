<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL;

use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\FunctionCallingHelper;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\FunctionCreationHelper;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\TableValidationHelper;
use Zend\ServiceManager\ServiceLocatorInterface;

class CodeGeneratorFactory
{
    public function __invoke(ServiceLocatorInterface $sl)
    {
        $service = new CodeGenerator(
            new TableValidationHelper(),
            new FunctionCreationHelper(),
            new FunctionCallingHelper()
        );

        return $service;
    }
}