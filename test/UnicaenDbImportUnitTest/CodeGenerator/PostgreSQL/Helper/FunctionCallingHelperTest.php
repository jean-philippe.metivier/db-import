<?php

namespace UnicaenDbImportUnitTest\CodeGenerator\PostgreSQL\Helper;

use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\FunctionCallingHelper;

class FunctionCallingHelperTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var FunctionCallingHelper
     */
    private $helper;
    
    protected function setUp()
    {
        $this->helper = new FunctionCallingHelper();
    }

    public function test_can_generateSQL()
    {
        $this->helper->setImportHash('import hash');

        $res = $this->helper->generateSQL(
            'create_import_metarequest_for_ztemptable',
            'code',
            ['libelle', 'debut_validite'],
            ['debut_validite' => "TO_CHAR(%s,'YYYY-MM-DD')"]
        );

        $expected = <<<'EOT'
create_import_metarequest_for_ztemptable(
    src.code, src.libelle, TO_CHAR(src.debut_validite,'YYYY-MM-DD'),
    dest.code, dest.libelle, TO_CHAR(dest.debut_validite,'YYYY-MM-DD'), dest.deleted_on,
    'import hash'
)
EOT;
        $this->assertEquals($expected, $res);
    }
}
