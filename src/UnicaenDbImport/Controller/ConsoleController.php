<?php

namespace UnicaenDbImport\Controller;

use UnicaenDbImport\Domain\Exception\ImportNotFoundException;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Service\ImportService;
use Zend\Console\ColorInterface;
use Zend\Console\Request;
use Zend\Mvc\Console\Controller\AbstractConsoleController;

class ConsoleController extends AbstractConsoleController
{
    /**
     * @var ImportService
     */
    private $importService;

    /**
     * @param ImportService $importService
     */
    public function setImportService(ImportService $importService)
    {
        $this->importService = $importService;
    }

    public function runImportAction()
    {
        /** @var Request $request */
        $request = $this->getRequest();

        $all = $request->getParam('all') || $request->getParam('a');
        $name = $request->getParam('name');

        if ($all && $name !== null) {
            $this->getConsole()->writeLine("Les paramètres 'all' et 'name' sont antinomiques", ColorInterface::RED);
        }

        if (!$all && $name === null) {
            $all = TRUE;
        }

        $this->getConsole()->writeLine();
        $this->getConsole()->writeLine("######################## IMPORTS ########################", ColorInterface::BLUE);
        $this->getConsole()->writeLine();

        try {
            if ($all) {
                $imports = $this->importService->getImports();
            } else {
                try {
                    $import = $this->importService->getImportByName($name);
                    $imports = [$import];
                }
                catch (ImportNotFoundException $exception) {
                    $imports = [];
                    $this->getConsole()->writeLine($exception->getMessage(), ColorInterface::RED);
                }
            }
        } catch (\Exception $exception) {
            $this->getConsole()->writeLine("Une erreur imprévue est survenue durant l'import !", ColorInterface::RED);
            throw $exception;
        }

        $this->runImports($imports);

        $this->getConsole()->writeLine("");
    }

    /**
     * @param ImportInterface[] $imports
     */
    private function runImports($imports)
    {
        if (count($imports) === 0) {
            $this->getConsole()->writeLine("Aucun import réalisé.", ColorInterface::NORMAL);

            return;
        }

        foreach ($imports as $import) {

            $this->getConsole()->writeLine("### " . $import->getName() . " ###", ColorInterface::BLACK, ColorInterface::WHITE);
            $this->getConsole()->writeLine(date_format(new \DateTime(), 'd/m/Y H:i:s'), ColorInterface::NORMAL);

            $result = $this->importService->runImport($import);

            if ($exception = $result->getException()) {
                $this->getConsole()->writeLine("Une erreur a été rencontrée!", ColorInterface::RED);
                $this->getConsole()->writeLine($exception->getMessage(), ColorInterface::RED);
                if ($previous = $exception->getPrevious()) {
                    $this->getConsole()->writeLine($previous->getMessage(), ColorInterface::RED);
                }
                $this->getConsole()->writeLine($exception->getTraceAsString(), ColorInterface::GRAY);
            }
            else {
                $this->getConsole()->writeLine((string)$result);
                $this->getConsole()->writeLine("Import réalisé avec succès.", ColorInterface::GREEN);
            }

            $this->getConsole()->writeLine();
        }
    }
}