<?php

namespace UnicaenDbImport\CodeGenerator\PostgreSQL\Helper;

use UnicaenDbImport\CodeGenerator\PostgreSQL\PostgreSQLCommonsTrait;

/**
 * Version PostgreSQL.
 *
 * @author Unicaen
 */
class FunctionCreationHelper extends \UnicaenDbImport\CodeGenerator\Common\Helper\FunctionCreationHelper
{
    use PostgreSQLCommonsTrait;

    protected function generateFunctionBodyBeginningSQLSnippet()
    {
        return <<<'EOT'
$Q$
DECLARE
    operation VARCHAR(64);
    hash VARCHAR(255);
    sql TEXT;
BEGIN
EOT;
    }

    protected function generateFunctionBodyEndingSQLSnippet()
    {
        return <<<'EOT'
END; $Q$
LANGUAGE plpgsql;
EOT;
    }
}