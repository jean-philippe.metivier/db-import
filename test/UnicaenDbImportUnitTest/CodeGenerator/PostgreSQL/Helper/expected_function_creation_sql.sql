CREATE OR REPLACE FUNCTION create_import_metarequest_for_ztemptable(
    src_code TEXT, src_libelle TEXT, src_debut_validite TEXT, src_fin_validite TEXT,
    dest_code TEXT, dest_libelle TEXT, dest_debut_validite TEXT, dest_fin_validite TEXT, dest_deleted_on TIMESTAMP(0) WITH TIME ZONE,
    import_hash VARCHAR(255)
) RETURNS VARCHAR(255) AS
$Q$
DECLARE
    operation VARCHAR(64);
    hash VARCHAR(255);
    sql TEXT;
BEGIN
    -- normalisation des valeurs d'entrée
    src_libelle = coalesce(src_libelle, 'null');
    src_debut_validite = coalesce(src_debut_validite, 'null');
    src_fin_validite = coalesce(src_fin_validite, 'null');
    dest_libelle = coalesce(dest_libelle, 'null');
    dest_debut_validite = coalesce(dest_debut_validite, 'null');
    dest_fin_validite = coalesce(dest_fin_validite, 'null');

    -- l'enregistrement existe dans la source mais pas dans la destination : il devra être ajouté
    IF (src_code IS NOT NULL AND dest_code IS NULL) THEN
        operation = 'insert';
        hash = src_code || '-' || import_hash;
        sql = 'INSERT INTO ztemptable(code, libelle, debut_validite, fin_validite, created_on) VALUES (' || quote_literal(src_code) || ', ' || quote_literal(src_libelle) || ', ' || quote_literal(src_debut_validite) || ', ' || quote_literal(src_fin_validite) || ', ' || 'LOCALTIMESTAMP(0)) ;' ;
        sql = sql || ' UPDATE import_reg SET executed_on = LOCALTIMESTAMP(0) WHERE import_hash = ' || quote_literal(hash) || ' ;' ;
        INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('insert', 'ztemptable', src_code, null, null, null, sql, LOCALTIMESTAMP(0), hash);
    END IF;

    -- l'enregistrement existe dans la destination et n'est pas historisé
    IF (src_code IS NOT NULL AND dest_code IS NOT NULL and dest_deleted_on IS NULL) THEN
        -- 'libelle' doit être mis à jour
        IF (src_libelle <> dest_libelle) THEN
            operation = 'update';
            hash = dest_code || '-' || dest_libelle || '-' || import_hash;
            sql = 'UPDATE ztemptable SET libelle = ' || quote_literal(src_libelle) || ', updated_on = LOCALTIMESTAMP(0) WHERE code = ' || quote_literal(dest_code) || ' ;' ;
            sql = sql || ' UPDATE import_reg SET executed_on = LOCALTIMESTAMP(0) WHERE import_hash = ' || quote_literal(hash) || ' ;' ;
            INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('update', 'ztemptable', src_code, 'libelle', src_libelle, dest_libelle, sql, LOCALTIMESTAMP(0), hash);
        END IF;
        -- 'debut_validite' doit être mis à jour
        IF (src_debut_validite <> dest_debut_validite) THEN
            operation = 'update';
            hash = dest_code || '-' || dest_debut_validite || '-' || import_hash;
            sql = 'UPDATE ztemptable SET debut_validite = ' || quote_literal(src_debut_validite) || ', updated_on = LOCALTIMESTAMP(0) WHERE code = ' || quote_literal(dest_code) || ' ;' ;
            sql = sql || ' UPDATE import_reg SET executed_on = LOCALTIMESTAMP(0) WHERE import_hash = ' || quote_literal(hash) || ' ;' ;
            INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('update', 'ztemptable', src_code, 'debut_validite', src_debut_validite, dest_debut_validite, sql, LOCALTIMESTAMP(0), hash);
        END IF;
        -- 'fin_validite' doit être mis à jour
        IF (src_fin_validite <> dest_fin_validite) THEN
            operation = 'update';
            hash = dest_code || '-' || dest_fin_validite || '-' || import_hash;
            sql = 'UPDATE ztemptable SET fin_validite = ' || quote_literal(src_fin_validite) || ', updated_on = LOCALTIMESTAMP(0) WHERE code = ' || quote_literal(dest_code) || ' ;' ;
            sql = sql || ' UPDATE import_reg SET executed_on = LOCALTIMESTAMP(0) WHERE import_hash = ' || quote_literal(hash) || ' ;' ;
            INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('update', 'ztemptable', src_code, 'fin_validite', src_fin_validite, dest_fin_validite, sql, LOCALTIMESTAMP(0), hash);
        END IF;

    END IF;

    -- l'enregistrement existe dans la destination mais historisé : il sera dé-historisé
    IF (src_code IS NOT NULL AND dest_code IS NOT NULL and dest_deleted_on IS NOT NULL) THEN
        operation = 'undelete';
        hash = dest_code || '-' || import_hash;
        sql = 'UPDATE ztemptable SET ' || 'libelle = ' || quote_literal(src_libelle) || ', ' || 'debut_validite = ' || quote_literal(src_debut_validite) || ', ' || 'fin_validite = ' || quote_literal(src_fin_validite) || ', ' || 'updated_on = LOCALTIMESTAMP(0), deleted_on = null WHERE code = ' || quote_literal(dest_code) || ' ;' ;
        sql = sql || ' UPDATE import_reg SET executed_on = LOCALTIMESTAMP(0) WHERE import_hash = ' || quote_literal(hash) || ' ;' ;
        INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('undelete', 'ztemptable', src_code, null, null, null, sql, LOCALTIMESTAMP(0), hash);
    END IF;

    -- l'enregistrement existe dans la destination mais plus dans la source : il sera historisé
    IF (src_code IS NULL AND dest_code IS NOT NULL and dest_deleted_on IS NULL) THEN
        operation = 'delete';
        hash = dest_code || '-' || import_hash;
        sql = 'UPDATE ztemptable SET deleted_on = LOCALTIMESTAMP(0) WHERE code = ' || quote_literal(dest_code) || ' ;' ;
        sql = sql || ' UPDATE import_reg SET executed_on = LOCALTIMESTAMP(0) WHERE import_hash = ' || quote_literal(hash) || ' ;' ;
        INSERT INTO import_reg(operation, table_name, source_code, field_name, to_value, from_value, sql, created_on, import_hash) VALUES ('delete', 'ztemptable', src_code, null, null, null, sql, LOCALTIMESTAMP(0), hash);
    END IF;

    RETURN operation;
END; $Q$
LANGUAGE plpgsql;