<?php

namespace UnicaenDbImport\Domain;

use Assert\AssertionChain;
use UnicaenDbImport\Config\ConfigException;
use Zend\Stdlib\Parameters;

class Import implements ImportInterface
{
    const INTERMEDIATE_TABLE_NAME_PREFIX = 'src_';

    /**
     * @var Parameters
     */
    protected $config;

    /**
     * @param array $config
     * @return static
     */
    static public function fromConfig(array $config)
    {
        $inst = new static($config);

        try {
            $inst->validateConfig();
        } catch (\Exception $e) {
            if (! $e instanceof ConfigException) {
                $e = new ConfigException($e->getMessage(), 0, $e);
            }
            throw $e;
        }

        return $inst;
    }

    protected function __construct(array $config)
    {
        $this->config = new Parameters($config);
    }

    protected function validateConfig()
    {
        (new AssertionChain($this->config->get($key = 'source'), "Une instance de " . SourceInterface::class . " est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->isInstanceOf(SourceInterface::class);

        (new AssertionChain($this->config->get($key = 'destination'), "Une instance de " . DestinationInterface::class . " est requise pour la clé suivante: $key"))
            ->notEmpty()
            ->isInstanceOf(DestinationInterface::class);

        if ($this->config->offsetExists($key = 'intermediate_table')) {
            $intermediateTable = $this->config->get($key = 'intermediate_table');
            (new AssertionChain($intermediateTable, "Une string non vide est requise pour la clé facultative suivante: $key"))
                ->notEmpty()
                ->string();
        }
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getName()
    {
        if ($name = $this->config->get('name')) {
            return $name;
        }

        return $this->getSource()->getName() . ' ==> ' . $this->getDestination()->getName();
    }

    /**
     * @return Source
     */
    public function getSource()
    {
        return $this->config->get('source');
    }

    /**
     * @return Destination
     */
    public function getDestination()
    {
        return $this->config->get('destination');
    }

    /**
     * @return string
     */
    public function getIntermediateTable()
    {
        if ($intermediatTable = $this->config->get('intermediate_table')) {
            return $intermediatTable;
        }

        return static::INTERMEDIATE_TABLE_NAME_PREFIX . $this->getDestination()->getTable();
    }

    /**
     * @return bool
     */
    public function getIntermediateTableAutoDrop()
    {
        return (bool) $this->config->get('intermediate_table_auto_drop', false);
    }

    /**
     * @return bool
     */
    public function requiresIntermediateTable()
    {
        $isSameConnectionForSourceAndDestination =
            $this->getSource()->getConnection() === $this->getDestination()->getConnection();

        return $isSameConnectionForSourceAndDestination === FALSE;
    }
}