<?php

namespace UnicaenDbImportUnitTest\CodeGenerator\PostgreSQL;

use Doctrine\DBAL\Connection;
use PHPUnit\Framework\TestCase;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\FunctionCallingHelper;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\FunctionCreationHelper;
use UnicaenDbImport\CodeGenerator\PostgreSQL\Helper\TableValidationHelper;
use UnicaenDbImport\CodeGenerator\PostgreSQL\CodeGenerator;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\Source;
use UnicaenDbImport\Domain\SourceInterface;

class CodeGeneratorTest extends TestCase
{
    /**
     * @var CodeGenerator
     */
    protected $codeGenerator;

    /** @var TableValidationHelper|\PHPUnit_Framework_MockObject_MockObject $tableValidationHelper */
    protected $tableValidationHelper;

    /** @var FunctionCreationHelper|\PHPUnit_Framework_MockObject_MockObject $functionCreationHelper */
    protected $functionCreationHelper;

    /** @var FunctionCallingHelper|\PHPUnit_Framework_MockObject_MockObject $functionCreationHelper */
    protected $functionCallingHelper;

    /**
     * @var SourceInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    protected $source;

    
    protected function setUp()
    {
        /** @var TableValidationHelper|\PHPUnit_Framework_MockObject_MockObject $tableValidationHelper */
        $this->tableValidationHelper = $this->createMock(TableValidationHelper::class);

        /** @var FunctionCreationHelper|\PHPUnit_Framework_MockObject_MockObject $functionCreationHelper */
        $this->functionCreationHelper = $this->createMock(FunctionCreationHelper::class);

        /** @var FunctionCreationHelper|\PHPUnit_Framework_MockObject_MockObject $functionCreationHelper */
        $this->functionCallingHelper = $this->createMock(FunctionCallingHelper::class);

        $this->codeGenerator = new CodeGenerator(
            $this->tableValidationHelper,
            $this->functionCreationHelper,
            $this->functionCallingHelper
        );
    }

    public function test_can_generate_sql_for_get_import_req_function()
    {
        /** @var \UnicaenDbImport\Domain\SourceInterface|\PHPUnit_Framework_MockObject_MockObject $source */
        $source = $this->createMock(SourceInterface::class);
        $source->expects($this->once())->method('getSourceCodeColumn')->willReturn('code');
        $source->expects($this->once())->method('getColumns')->willReturn(['libelle', 'debut_validite', 'fin_validite']);

        /** @var \UnicaenDbImport\Domain\DestinationInterface|\PHPUnit_Framework_MockObject_MockObject $destination */
        $destination = $this->createMock(DestinationInterface::class);
        $destination->expects($this->once())->method('getTable')->willReturn('ztemptable');

        $this->functionCreationHelper->expects($this->once())->method('generateSQL')->willReturn('FUNCTION_CREATION_SQL');

        $sql = $this->codeGenerator->generateImportMetaRequestFunctionCreationSQL($source, $destination);

        $this->assertEquals('FUNCTION_CREATION_SQL', $sql);
    }

    public function test_can_generate_sql_for_intermediate_table_creation()
    {
        /** @var SourceInterface|\PHPUnit_Framework_MockObject_MockObject $source */
        $source = $this->createMock(SourceInterface::class);
        $source->expects($this->once())->method('getSourceCodeColumn')->willReturn('code');
        $source->expects($this->once())->method('getColumns')->willReturn(['libelle', 'debut_validite', 'fin_validite']);

        /** @var \UnicaenDbImport\Domain\DestinationInterface|\PHPUnit_Framework_MockObject_MockObject $destination */
        $destination = $this->createMock(DestinationInterface::class);
        $destination->expects($this->once())->method('getTable')->willReturn('ztemptable');

        $sql = $this->codeGenerator->generateSQLForIntermediateTableCreation($source, $destination, 'src_ztemptable');
        $expectedSql = file_get_contents(__DIR__ . '/expected_sql_for_intermediate_table_creation.sql');

        $this->assertEquals($expectedSql, $sql);
    }

    public function test_can_generate_sql_for_select_from_source()
    {
        $source = Source::fromConfig([
            'name'               => 'ztemptable',
            'table'              => 'ztemptable',
            'connection'         => $this->createMock(Connection::class),
            'source_code_column' => 'code',
            'columns'            => ['libelle', 'debut_validite', 'fin_validite'],
        ]);

        $sql = $this->codeGenerator->generateSQLForSelectFromSource($source);
        $expectedSql = file_get_contents(__DIR__ . '/expected_sql_for_select_from_source.sql');

        $this->assertEquals($expectedSql, $sql);
    }

    public function test_can_generate_sql_for_select_from_source_specifying_sql_select()
    {
        $source = Source::fromConfig([
            'name'               => 'ztemptable',
            'select'             => 'SELECT ...',
            'connection'         => $this->createMock(Connection::class),
            'source_code_column' => 'code',
            'columns'            => ['libelle', 'debut_validite', 'fin_validite'],
        ]);

        $sql = $this->codeGenerator->generateSQLForSelectFromSource($source);

        $this->assertEquals('SELECT ...', $sql);
    }

    public function test_can_generate_sql_for_insert_into_intermediate_table()
    {
        $sql = $this->codeGenerator->generateSQLForInsertIntoIntermmediateTable('src_ztemptable', [
            'code'           => 'A006',
            'libelle'        => "Libellé d'aujourd'hui (NB: l'apostrophe)",
            'debut_validite' => '2017-03-11',
            'fin_validite'   => '2018-03-11',
        ]);
        $expectedSql = file_get_contents(__DIR__ . '/expected_sql_for_insert_into_intermediate_table.sql');

        $this->assertEquals($expectedSql, $sql);
    }

    public function test_can_generate_sql_for_full_outer_join_select()
    {
        /** @var SourceInterface|\PHPUnit_Framework_MockObject_MockObject $source */
        $source = $this->createMock(SourceInterface::class);
        $source->expects($this->once())->method('getTable')->willReturn('src_ztemptable');

        /** @var DestinationInterface|\PHPUnit_Framework_MockObject_MockObject $destination */
        $destination = $this->createMock(DestinationInterface::class);
        $destination->expects($this->once())->method('getTable')->willReturn('ztemptable');
        $destination->expects($this->once())->method('getSourceCodeColumn')->willReturn('code');
        $destination->expects($this->once())->method('getColumns')->willReturn(['libelle', 'debut_validite', 'fin_validite']);
        $destination->expects($this->once())->method('getColumnsToChar')->willReturn([]);

        $this->functionCallingHelper->expects($this->once())->method('generateSQL')->willReturn('FUNCTION_CALLING_SQL');

        $sql = $this->codeGenerator->generateSourceAndDestinationDiffSelectSQL($source, $destination, null, 'import hash');
        $expectedSql = file_get_contents(__DIR__ . '/expected_sql_for_table_sync_select_req.sql');

        $this->assertEquals($expectedSql, $sql);
    }
}