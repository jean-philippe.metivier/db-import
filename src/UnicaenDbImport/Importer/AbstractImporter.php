<?php

namespace UnicaenDbImport\Importer;

use UnicaenDbImport\DatabaseFacade;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\ImportResult;

abstract class AbstractImporter implements ImporterInterface
{
    /**
     * @var string
     */
    protected $intermediateTable;

    /**
     * @var \UnicaenDbImport\Domain\ImportInterface
     */
    protected $import;

    /**
     * @var DatabaseFacade
     */
    protected $databaseFacade;

    /**
     * AbstractImporter constructor.
     *
     * @param DatabaseFacade $databaseFacade
     */
    public function __construct(DatabaseFacade $databaseFacade)
    {
        $this->databaseFacade = $databaseFacade;
    }

    /**
     * @param ImportInterface $import
     * @return static
     */
    public function setImport(ImportInterface $import)
    {
        $this->import = $import;

        return $this;
    }

    /**
     * Réalise l'import.
     *
     * @return ImportResult Résultat de l'import
     */
    public function run()
    {
        $source = $this->import->getSource();
        $destination = $this->import->getDestination();

        $this->intermediateTable = null;

        try {
            $this->databaseFacade->validateDestinationTable($destination);

            if ($this->import->requiresIntermediateTable()) {
                $this->intermediateTable = $this->import->getIntermediateTable();
                if ($this->import->getIntermediateTableAutoDrop()) {
                    $this->databaseFacade->dropIntermediateTable($this->intermediateTable, $destination);
                } else {
                    $this->databaseFacade->checkIntermediateTableNotExists($destination, $this->intermediateTable);
                }
                $this->databaseFacade->createIntermediateTable($this->intermediateTable, $source, $destination);
                $this->databaseFacade->populateIntermediateTable($this->intermediateTable, $source, $destination);
            } else {
                $this->databaseFacade->validateSourceTable($source);
            }

            $this->databaseFacade->createMetaRequestCreationFunction($source, $destination);
            $this->databaseFacade->createImportRegTableIfNotExists($destination);
            $this->databaseFacade->executeDiffRequest($source, $destination, $this->intermediateTable);

            if ($this->import->requiresIntermediateTable()) {
                $this->databaseFacade->dropIntermediateTable($this->intermediateTable, $destination);
            }

            $arrayResults = $this->databaseFacade->fetchImportRegister($destination);

            $result = ImportResult::fromImportRegisterFetchResult($arrayResults);
            $result->setImportName($this->import->getName());

            $sql = $result->getSQL();
            if ($sql !== '') {
                $this->databaseFacade->executeImportRequests($sql, $destination);
            }
        } catch (\Exception $e) {
            $result = new ImportResult();
            $result->setImportName($this->import->getName());
            $result->setException($e);
        }

        return $result;
    }
}