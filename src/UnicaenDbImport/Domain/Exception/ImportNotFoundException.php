<?php

namespace UnicaenDbImport\Domain\Exception;

use UnicaenApp\Exception\RuntimeException;

class ImportNotFoundException extends RuntimeException
{
    public static function byName($name)
    {
        return new static("Import introuvable avec ce nom : " . $name);
    }
}