<?php

namespace UnicaenDbImport;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Event\Listeners\OracleSessionInit;
use Doctrine\DBAL\Platforms\OraclePlatform;
use DoctrineORMModule\Options\EntityManager;
use UnicaenApp\Exception\RuntimeException;
use UnicaenDbImport\CodeGenerator\Common\AbstractCodeGenerator;
use UnicaenDbImport\Domain\DestinationInterface;
use UnicaenDbImport\Domain\SourceInterface;

class DatabaseFacade
{
    /**
     * @var AbstractCodeGenerator
     */
    protected $codeGenerator;

    /**
     * @var QueryExecutor
     */
    protected $queryExecutor;

    /**
     * DatabaseHelper constructor.
     *
     * @param AbstractCodeGenerator $codeGenerator
     * @param QueryExecutor         $queryExecutor
     */
    public function __construct(AbstractCodeGenerator $codeGenerator, QueryExecutor $queryExecutor)
    {
        $this->codeGenerator = $codeGenerator;
        $this->queryExecutor = $queryExecutor;
    }

    public function validateSourceTable(SourceInterface $source)
    {
        $connection = $source->getConnection();
        $table = $source->getTable();
        $columns = $source->getColumns();

        // test if table exists
        $sql = $this->codeGenerator->generateSQLForTableExistenceCheck($table);
        $result = $this->queryExecutor->fetchAll($sql, $connection);
        $exists = $this->codeGenerator->convertTableExistenceCheckResultToBoolean($result);
        if (!$exists) {
            throw new RuntimeException("La table source '$table' est introuvable. ");
        }

        // data columns validation
        $sql = $this->codeGenerator->generateSQLForDataColumnsValidation($table, $columns);
        $result = $this->queryExecutor->fetchAll($sql, $connection);
        $exception = $this->codeGenerator->convertDataColumnsValidationBadResultToException($table, $result, $columns);
        if ($exception !== null) {
            throw new RuntimeException("La table source '$table' ou la config n'est pas valide. ", 0, $exception);
        }
    }

    public function validateDestinationTable(DestinationInterface $destination)
    {
        $connection = $destination->getConnection();
        $table = $destination->getTable();
        $columns = $destination->getColumns();

        // test if table exists
        $sql = $this->codeGenerator->generateSQLForTableExistenceCheck($table);
        $result = $this->queryExecutor->fetchAll($sql, $connection);
        $exists = $this->codeGenerator->convertTableExistenceCheckResultToBoolean($result);
        if (!$exists) {
            throw new RuntimeException("La table destination '$table' est introuvable. ");
        }

        // data columns validation
        $sql = $this->codeGenerator->generateSQLForDataColumnsValidation($table, $columns);
        $result = $this->queryExecutor->fetchAll($sql, $connection);
        $exception = $this->codeGenerator->convertDataColumnsValidationBadResultToException($table, $result, $columns);
        if ($exception !== null) {
            throw new RuntimeException("La table destination '$table' ou la config n'est pas valide. ", 0, $exception);
        }

        // histo columns validation
        $sql = $this->codeGenerator->generateSQLForHistoColumnsValidation($table);
        $result = $this->queryExecutor->fetchAll($sql, $connection);
        $exception = $this->codeGenerator->convertHistoColumnsValidationBadResultToException($table, $result);
        if ($exception !== null) {
            throw new RuntimeException("La table '$table' n'est pas une table de destination valide. ", 0, $exception);
        }
    }

    public function createImportRegTableIfNotExists(DestinationInterface $destination)
    {
        $connection = $destination->getConnection();

        // test if table exists
        $sql = $this->codeGenerator->generateSQLForTableExistenceCheck('import_reg');
        $result = $this->queryExecutor->fetchAll($sql, $connection);
        $exists = $this->codeGenerator->convertTableExistenceCheckResultToBoolean($result);
        if ($exists) {
            return;
        }

        // create table
        $sql = $this->codeGenerator->generateSQLForImportRegTableCreation();
        $this->queryExecutor->exec($sql, $connection);
    }

    /**
     * @param SourceInterface      $source
     * @param DestinationInterface $destination
     * @param string               $intermediateTable
     * @return array
     */
    public function executeDiffRequest(SourceInterface $source, DestinationInterface $destination, $intermediateTable = null)
    {
        $sql = $this->codeGenerator->generateSourceAndDestinationDiffSelectSQL(
            $source,
            $destination,
            $intermediateTable ?: null);
        $result = $this->queryExecutor->fetchAll($sql, $destination->getConnection());

        return $result;
    }

    public function fetchImportRegister(DestinationInterface $destination)
    {
        $sql = $this->codeGenerator->generateSQLForFetchingImportRequestsToExecute();
        $result = $this->queryExecutor->fetchAll($sql, $destination->getConnection());

        return $result;
    }

    /**
     * @param string               $sql
     * @param DestinationInterface $destination
     */
    public function executeImportRequests($sql, DestinationInterface $destination)
    {
        $this->queryExecutor->exec($sql, $destination->getConnection());
    }

    public function createMetaRequestCreationFunction(SourceInterface $source, DestinationInterface $destination)
    {
        $sql = $this->codeGenerator->generateImportMetaRequestFunctionCreationSQL($source, $destination);

        $this->queryExecutor->exec($sql, $destination->getConnection());
    }

    public function checkIntermediateTableNotExists(DestinationInterface $destination, $intermediateTable)
    {
        $sql = $this->codeGenerator->generateSQLForTableExistenceCheck($intermediateTable);
        $result = $this->queryExecutor->fetchAll($sql, $destination->getConnection());
        $exists = $this->codeGenerator->convertTableExistenceCheckResultToBoolean($result);
        if ($exists) {
            throw new RuntimeException(
                "Une table $intermediateTable existe déjà. Veuillez la supprimer ou alors spécifier dans la config " .
                "le nom à utiliser pour générer la table intermédiaire nécessaire à l'import dans la table " .
                $destination->getTable());
        }
    }

    public function createIntermediateTable($intermediateTable, SourceInterface $source, DestinationInterface $destination)
    {
        $sql = $this->codeGenerator->generateSQLForIntermediateTableCreation($source, $destination, $intermediateTable);

        $this->queryExecutor->exec($sql, $destination->getConnection());
    }

    public function dropIntermediateTable($intermediateTable, DestinationInterface $destination)
    {
        $sql = $this->codeGenerator->generateSQLForIntermmediateTableDrop($intermediateTable);

        $this->queryExecutor->exec($sql, $destination->getConnection());
    }

    public function populateIntermediateTable($intermediateTable, SourceInterface $source, DestinationInterface $destination)
    {
        $columns = $source->getColumns();
        $sourceCodeColumn = $source->getSourceCodeColumn();

        $selectSql = $this->codeGenerator->generateSQLForSelectFromSource($source);

        //
        // Inscription manuelle du listener OracleSessionInit.
        // TODO: trouver pourquoi l'inscription automatique ne fonctionne pas.
        //
        try {
            $sourcePlatform = $source->getConnection()->getDatabasePlatform();
        } catch (DBALException $e) {
            throw new RuntimeException("Impossible de déterminer le type de SGBD", null, $e);
        }
        if ($sourcePlatform instanceof OraclePlatform) {
            $a = $source->getConnection();
            $a->getEventManager()->addEventSubscriber(new OracleSessionInit());
            $result = $this->queryExecutor->fetchAll($selectSql, $a);
        }
        //

        $insertsSQL = '';
        foreach ($result as $row) {
            $columns = array_merge([$sourceCodeColumn], $columns);
            $columnsValues = [];
            foreach ($columns as $column) {
                if (!array_key_exists($column, $row)) {
                    $column = strtoupper($column);
                }
                $columnsValues[$column] = $row[$column];
            }
            $insertSql = $this->codeGenerator->generateSQLForInsertIntoIntermmediateTable($intermediateTable, $columnsValues);

            $insertsSQL .= $insertSql . PHP_EOL;
        }

        // insert into destination
        $this->queryExecutor->exec($insertsSQL, $destination->getConnection());
    }
}