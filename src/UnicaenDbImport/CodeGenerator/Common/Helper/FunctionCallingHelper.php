<?php

namespace UnicaenDbImport\CodeGenerator\Common\Helper;

use Ramsey\Uuid\Uuid;

/**
 * Génération de code SQL pour l'appel de la fonction "create_import_metarequest_for_{table}".
 *
 * Version commune à toutes les plateformes de bases de données.
 *
 * @author Unicaen
 */
abstract class FunctionCallingHelper extends AbstractHelper
{
    /**
     * @var string
     */
    protected $importHash;

    /**
     * @param string $importHash
     */
    public function setImportHash($importHash)
    {
        $this->importHash = $importHash;
    }

    /**
     * @param string $functionName
     * @param string $sourceCodeColumn
     * @param array  $columns
     * @param array  $columnsToChar
     * @return string
     */
    public function generateSQL($functionName, $sourceCodeColumn, array $columns, array $columnsToChar)
    {
        $sourceCols = array_merge([$sourceCodeColumn], $columns);
        $destinCols = array_merge([$sourceCodeColumn], $columns, ['deleted_on']);

        $sourceCols = $this->generateColumnNamesWithPrefix($sourceCols, 'src.',  $columnsToChar);
        $destinCols = $this->generateColumnNamesWithPrefix($destinCols, 'dest.', $columnsToChar);

        $importHash = $this->importHash ?: Uuid::uuid1()->toString();

        $res = '';
        $res .= $functionName . '(' . PHP_EOL;
        $res .= $this->indent(4, implode(', ', $sourceCols) . ',') . PHP_EOL;
        $res .= $this->indent(4, implode(', ', $destinCols) . ',') . PHP_EOL;
        $res .= $this->indent(4, "'" . $importHash . "'") . PHP_EOL;
        $res .= ")";

        return $res;
    }

    /**
     * @param array  $columns
     * @param string $prefix
     * @param array  $columnsToChar
     * @return array
     */
    protected function generateColumnNamesWithPrefix(array $columns, $prefix, array $columnsToChar = [])
    {
        return array_map(function ($col) use ($prefix, $columnsToChar) {
            $field = $prefix . $col;
            // Attention: $columnsToChar[$col] doit être un motif du genre "TO_CHAR(%s,'YYYY-MM-DD')"
            // où "%s" sera remplacé par le nom de la colonne.
            if (array_key_exists(strtolower($col), $columnsToChar) || array_key_exists(strtoupper($col), $columnsToChar)) {
                $field = sprintf($columnsToChar[$col], $field);
            }
            return $field;
        }, $columns);
    }
}