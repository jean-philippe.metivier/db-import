<?php

namespace UnicaenDbImport\Config;

use UnicaenApp\Exception\RuntimeException;

class ConfigException extends RuntimeException
{
    public static function missingKey($key)
    {
        return new static("Clé de config '$key' introuvable");
    }

    public static function exclusiveKeys(array $keys)
    {
        return new static("Les clés de config suivantes ne peuvent être présentes toutes les deux: " . implode(', ', $keys));
    }

    public static function atLeastOneKey(array $keys)
    {
        return new static("L'une des clés de config suivantes doit être présente: " . implode(', ', $keys));
    }

    public static function illegalKey($key)
    {
        return new static("La clé de config '$key' n'est pas autorisée");
    }
}