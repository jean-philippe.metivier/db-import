<?php

namespace UnicaenDbImport;

use Zend\Console\Adapter\AdapterInterface;
use Zend\Console\Console;
use Zend\ModuleManager\Feature\ConsoleUsageProviderInterface;
use Zend\Mvc\Application;
use Zend\Mvc\MvcEvent;

class Module implements ConsoleUsageProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        /** @var Application $application */
//        $application = $e->getParam('application');
//        $eventManager = $application->getEventManager();
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }

    public function getConsoleUsage(AdapterInterface $console)
    {
        return [
            // Describe available commands
//            'user resetpassword [--verbose|-v] EMAIL' => 'Reset password for a user',
            'run-imports' => 'Lance les imports',

//            // Describe expected parameters
//            ['EMAIL', 'Email of the user for a password reset'],
//            ['--verbose|-v', '(optional) turn on verbose mode'],
        ];
    }
}
