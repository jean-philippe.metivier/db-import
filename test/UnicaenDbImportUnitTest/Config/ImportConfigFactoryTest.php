<?php

namespace UnicaenDbImportUnitTest\Service;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use UnicaenDbImport\Config\ImportConfig;
use UnicaenDbImport\Config\ImportConfigFactory;
use UnicaenDbImport\Domain\Destination;
use UnicaenDbImport\Domain\Import;
use UnicaenDbImport\Domain\Source;
use UnicaenDbImport\Importer\PostgreSQL\PostgreSQLImporter;
use PHPUnit_Framework_TestCase;
use Zend\ServiceManager\ServiceLocatorInterface;

class ImportConfigFactoryTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var ImportConfigFactory
     */
    private $factory;

    protected function setUp()
    {
        parent::setUp();

        $this->factory = new ImportConfigFactory();
    }

    /**
     * @expectedException \UnicaenDbImport\Config\ConfigException
     */
    public function test_requires_config_base_key()
    {
        $appConfig = ['...'];

        $sl = $this->createServiceLocatorMock([
            ['Config', $appConfig],
        ]);

        $this->factory->__invoke($sl);
    }

    /**
     * @expectedException \UnicaenDbImport\Config\ConfigException
     */
    public function test_throws_exception_if_importers_key_not_found()
    {
        $appConfig = [
            'import' => [],
        ];

        $sl = $this->createServiceLocatorMock([
            ['Config', $appConfig],
        ]);

        $this->factory->__invoke($sl);
    }

    /**
     * @expectedException \UnicaenDbImport\Importer\ImporterException
     */
    public function test_throws_exception_if_importer_is_not_found_by_service_locator()
    {
        $appConfig = require 'config.php';

        $sl = $this->createServiceLocatorMock([
            ['Config', $appConfig],
        ]);
        $sl->expects($this->once())->method('has')->with(PostgreSQLImporter::class)->willReturn(false);

        $this->factory->__invoke($sl);
    }

    /**
     * @dataProvider getInvalidImportConfig
     * @expectedException \UnicaenDbImport\Config\ConfigException
     * @param array $invalidImportConfig
     */
    public function test_throws_exception_if_import_config_is_invalid(array $invalidImportConfig)
    {
        $appConfig = [
            'import' => [
                'importers' => [
                    PostgreSqlPlatform::class => PostgreSQLImporter::class,
                ],
                'imports' => [
                    $invalidImportConfig,
                ],
            ],
        ];

        $sl = $this->createServiceLocatorMock([
            ['Config', $appConfig],
            [PostgreSQLImporter::class, $this->createMock(PostgreSQLImporter::class)],
        ]);
        $sl->expects($this->once())->method('has')->with(PostgreSQLImporter::class)->willReturn(true);

        $this->factory->__invoke($sl);
    }

    public function getInvalidImportConfig()
    {
        return [
            'destination_is_missing' => [
                ['source' => ['...']]
            ],
            'source_is_missing' => [
                ['destination' => ['...']]
            ],
        ];
    }

    public function test_creation_of_config()
    {
        $appConfig = require 'config.php';

        $sl = $this->createServiceLocatorMock([
            ['Config', $appConfig],
            ['doctrine.connection.orm_default', $this->createMock(Connection::class)],
            ['doctrine.connection.orm_sifac', $this->createMock(Connection::class)],
            [PostgreSQLImporter::class, $this->createMock(PostgreSQLImporter::class)],
        ]);
        $sl->expects($this->once())->method('has')->with(PostgreSQLImporter::class)->willReturn(true);

        $config = $this->factory->__invoke($sl);

        $this->assertInstanceOf(ImportConfig::class, $config);

        $imports = $config->getImports();

        $this->assertCount(2, $imports);
        $this->assertContainsOnlyInstancesOf(Import::class, $imports);

        /** @var Import $import */
        foreach ($imports as $import) {
            $this->assertInstanceOf(Import::class, $import);
            $this->assertInstanceOf(Source::class, $import->getSource());
            $this->assertInstanceOf(Destination::class, $import->getDestination());

            $this->assertInstanceOf(Connection::class, $import->getSource()->getConnection());
        }
    }

    private function createServiceLocatorMock(array $getMethodReturnValueMap)
    {
        /** @var \PHPUnit_Framework_MockObject_MockObject|ServiceLocatorInterface $sl */
        $sl = $this->getMockForAbstractClass(ServiceLocatorInterface::class);
        $sl->expects($this->atLeastOnce())->method('get')->willReturnMap($getMethodReturnValueMap);

        return $sl;
    }
}