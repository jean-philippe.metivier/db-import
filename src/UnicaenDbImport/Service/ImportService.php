<?php

namespace UnicaenDbImport\Service;

use UnicaenDbImport\Config\ImportConfig;
use UnicaenDbImport\Domain\Exception\ImportNotFoundException;
use UnicaenDbImport\Domain\ImportInterface;
use UnicaenDbImport\Domain\ImportResult;

class ImportService
{
    /**
     * @var ImportConfig
     */
    private $config;

    /**
     * ImportService constructor.
     *
     * @param ImportConfig $config
     */
    function __construct(ImportConfig $config)
    {
        $this->config = $config;
    }

    /**
     * Retourne tous les imports issus de la config.
     *
     * @return ImportInterface[]
     */
    public function getImports()
    {
        return $this->config->getImports();
    }

    /**
     * Lance tous les imports.
     *
     * @return ImportResult[] Agrégat des résultats des l'exécutions des imports
     */
    public function runAllImports()
    {
        $imports = $this->config->getImports();
        $results = [];

        foreach ($imports as $import) {
            $results[$import->getName()] = $this->runImport($import);
        }

        return $results;
    }

    /**
     * Retourne l'import dont le nom est spécifié.
     *
     * @param string $name Nom de l'import (name)
     * @return ImportInterface Import trouvé
     *
     * @throws ImportNotFoundException Import introuvable
     */
    public function getImportByName($name)
    {
        return $this->config->getImport($name);
    }

    /**
     * Lance l'import spécifié.
     *
     * @param ImportInterface $import
     * @return ImportResult Résultat de l'exécution de l'import
     */
    public function runImport(ImportInterface $import)
    {
        $importer = $this->config->getImporterForImport($import);
        $importer->setImport($import);
        $result = $importer->run();

        return $result;
    }
}